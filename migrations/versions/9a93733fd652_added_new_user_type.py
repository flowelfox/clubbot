"""Added new user type

Revision ID: 9a93733fd652
Revises: da1a064ea0ce
Create Date: 2020-02-11 17:29:23.356271

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9a93733fd652'
down_revision = 'da1a064ea0ce'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute("COMMIT")
    op.execute("ALTER TYPE participanttype ADD VALUE 'associated_member'")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    print("Remove associated_member manually")
    # ### end Alembic commands ###
