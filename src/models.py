import datetime
import enum

from botmanlib.menus.helpers import translator
from botmanlib.models import Database, BaseUser, UserPermissionsMixin, BasePermission, BaseUserSession, UserSessionsMixin, ModelPermissionsBase
from sqlalchemy import Column, String, Integer, ForeignKey, UniqueConstraint, ARRAY, DateTime, Table
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.orm import object_session, relationship

database = Database()
Base = database.Base

"""=====================ENUMS======================"""


class ParticipantType(enum.Enum):
    full_member = 'full_member'
    honorable_member = 'honorable_member'
    business_partner = 'business_partner'
    organ_of_state_power = 'organ_of_state_power'
    associated_member = 'associated_member'

    def to_str(self, lang):
        _ = translator(lang)
        if self is ParticipantType.full_member:
            return _("Full member")
        elif self is ParticipantType.honorable_member:
            return _("Honorable member")
        elif self is ParticipantType.business_partner:
            return _("Business partner")
        elif self is ParticipantType.organ_of_state_power:
            return _("Organ of state power")
        elif self is ParticipantType.associated_member:
            return _("Associated member")
        else:
            return _("Unknown")


"""=====================ASSOCIATION TABLES======================"""

# NEWS
news_clubs_assoc_association_table = Table('news_clubs_assoc', Base.metadata,
                                           Column('news_id', Integer, ForeignKey('news.id')),
                                           Column('club_id', Integer, ForeignKey('clubs.id'))
                                           )
news_users_liked_assoc_association_table = Table('news_users_liked_assoc', Base.metadata,
                                                 Column('news_id', Integer, ForeignKey('news.id')),
                                                 Column('user_id', Integer, ForeignKey('users.id'))
                                                 )
news_users_disliked_assoc_association_table = Table('news_users_disliked_assoc', Base.metadata,
                                                    Column('news_id', Integer, ForeignKey('news.id')),
                                                    Column('user_id', Integer, ForeignKey('users.id'))
                                                    )
news_users_interested_in_assoc_association_table = Table('news_users_interested_in_assoc', Base.metadata,
                                                         Column('news_id', Integer, ForeignKey('news.id')),
                                                         Column('user_id', Integer, ForeignKey('users.id'))
                                                         )

# EVENTS
events_clubs_assoc_association_table = Table('events_clubs_assoc', Base.metadata,
                                             Column('event_id', Integer, ForeignKey('events.id')),
                                             Column('club_id', Integer, ForeignKey('clubs.id'))
                                             )
events_users_liked_assoc_association_table = Table('events_users_liked_assoc', Base.metadata,
                                                   Column('event_id', Integer, ForeignKey('events.id')),
                                                   Column('user_id', Integer, ForeignKey('users.id'))
                                                   )
events_users_disliked_assoc_association_table = Table('events_users_disliked_assoc', Base.metadata,
                                                      Column('event_id', Integer, ForeignKey('events.id')),
                                                      Column('user_id', Integer, ForeignKey('users.id'))
                                                      )
events_users_interested_in_assoc_association_table = Table('events_users_interested_in_assoc', Base.metadata,
                                                           Column('event_id', Integer, ForeignKey('events.id')),
                                                           Column('user_id', Integer, ForeignKey('users.id'))
                                                           )
# OFFERS
offers_clubs_assoc_association_table = Table('offers_clubs_assoc', Base.metadata,
                                             Column('offer_id', Integer, ForeignKey('offers.id')),
                                             Column('club_id', Integer, ForeignKey('clubs.id'))
                                             )
offers_users_liked_assoc_association_table = Table('offers_users_liked_assoc', Base.metadata,
                                                   Column('offer_id', Integer, ForeignKey('offers.id')),
                                                   Column('user_id', Integer, ForeignKey('users.id'))
                                                   )
offers_users_disliked_assoc_association_table = Table('offers_users_disliked_assoc', Base.metadata,
                                                      Column('offer_id', Integer, ForeignKey('offers.id')),
                                                      Column('user_id', Integer, ForeignKey('users.id'))
                                                      )
offers_users_interested_in_assoc_association_table = Table('offers_users_interested_in_assoc', Base.metadata,
                                                           Column('offer_id', Integer, ForeignKey('offers.id')),
                                                           Column('user_id', Integer, ForeignKey('users.id'))
                                                           )

"""=====================MODELS======================"""


class User(Base, BaseUser, UserPermissionsMixin, UserSessionsMixin):
    __tablename__ = 'users'

    desired_name = Column(String)
    phone = Column(String)
    region = relationship("Region", back_populates="users")
    region_id = Column(Integer, ForeignKey('regions.id'))

    club_requests = relationship("JoinClubRequest", back_populates="user")
    news = relationship("News", back_populates="user")
    events = relationship("Event", back_populates="user")
    offers = relationship("Offer", back_populates="user")

    liked_news = relationship("News", secondary=news_users_liked_assoc_association_table, back_populates="likes")
    disliked_news = relationship("News", secondary=news_users_disliked_assoc_association_table, back_populates="dislikes")
    interested_in_news = relationship("News", secondary=news_users_interested_in_assoc_association_table, back_populates="interested_in")

    liked_events = relationship("Event", secondary=events_users_liked_assoc_association_table, back_populates="likes")
    disliked_events = relationship("Event", secondary=events_users_disliked_assoc_association_table, back_populates="dislikes")
    interested_in_events = relationship("Event", secondary=events_users_interested_in_assoc_association_table, back_populates="interested_in")

    liked_offers = relationship("Offer", secondary=offers_users_liked_assoc_association_table, back_populates="likes")
    disliked_offers = relationship("Offer", secondary=offers_users_disliked_assoc_association_table, back_populates="dislikes")
    interested_in_offers = relationship("Offer", secondary=offers_users_interested_in_assoc_association_table, back_populates="interested_in")

    reports = relationship("Report", back_populates="user")

    def init_permissions(self):
        session = object_session(self)
        if session is None:
            session = database.DBSession
        for permission in ['start_menu_access',
                           'profile_menu_access',
                           'change_language_menu_access',
                           'clubs_menu_access',
                           'join_club_menu_access',
                           'waiting_assignment_menu_access',
                           'my_clubs_menu_access',
                           'on_moderation_menu_access',
                           'digest_menu_access',
                           'view_news_menu_access',
                           'view_events_menu_access',
                           'view_offers_menu_access',
                           'announcement_menu_access',
                           'allow_create_news',
                           'allow_create_events',
                           'allow_create_offers']:
            perm = session.query(Permission).get(permission)
            if perm not in self.permissions:
                self.permissions.append(perm)

    @property
    def registered(self):
        return self.desired_name and self.phone and self.region

    @property
    def clubs(self):
        session = object_session(self)

        return session.query(Club) \
            .join(Club.requests) \
            .filter(JoinClubRequest.user == self) \
            .filter(JoinClubRequest.type != None) \
            .order_by(Club.title.asc()) \
            .all()


class Permission(BasePermission, Base):
    __tablename__ = 'permissions'


class UserSession(BaseUserSession, Base):
    __tablename__ = 'user_sessions'


class Region(Base, ModelPermissionsBase):
    __tablename__ = 'regions'

    id = Column(Integer, primary_key=True)
    translations = relationship("RegionTranslation", back_populates='region', cascade='all,delete')
    languages = Column(ARRAY(String), default=['ru', 'uk'])
    users = relationship("User", back_populates='region')
    clubs = relationship("Club", back_populates='region')

    def get_translation(self, lang):
        trans = DBSession.query(RegionTranslation).filter(RegionTranslation.region_id == self.id).filter(RegionTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(RegionTranslation).filter(RegionTranslation.region_id == self.id).first()
        return trans


class RegionTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'region_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    region_id = Column(Integer, ForeignKey('regions.id'), nullable=False)
    region = relationship('Region', back_populates='translations')
    name = Column(String, nullable=False)
    name_declension = Column(String, nullable=False)

    __table_args__ = (UniqueConstraint('region_id', 'lang', name='_region_lang_uc'),)


class Club(Base, ModelPermissionsBase):
    __tablename__ = 'clubs'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    link = Column(String, nullable=True)
    image = Column(String, nullable=True)
    region = relationship("Region", back_populates="clubs")
    region_id = Column(Integer, ForeignKey('regions.id'), nullable=False)
    requests = relationship("JoinClubRequest", back_populates="club")
    news = relationship("News", secondary=news_clubs_assoc_association_table, back_populates="clubs")
    events = relationship("Event", secondary=events_clubs_assoc_association_table, back_populates="clubs")
    offers = relationship("Offer", secondary=offers_clubs_assoc_association_table, back_populates="clubs")


class JoinClubRequest(Base, ModelPermissionsBase):
    __tablename__ = 'join_club_requests'

    id = Column(Integer, primary_key=True)
    edrpou = Column(String, nullable=False)
    company = Column(String, nullable=False)
    city = Column(String, nullable=False)
    owner = Column(String, nullable=False)
    name = Column(String, nullable=False)
    phone = Column(String, nullable=False)
    email = Column(String, nullable=True)
    assignment = Column(String, nullable=True)
    type = Column(ENUM(ParticipantType), nullable=True)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)

    club = relationship("Club", back_populates="requests")
    club_id = Column(Integer, ForeignKey('clubs.id'), nullable=False)

    user = relationship("User", back_populates="club_requests")
    user_id = Column(Integer, ForeignKey('users.id'))


class News(Base, ModelPermissionsBase):
    __tablename__ = 'news'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    text = Column(String, nullable=False)
    image = Column(String, nullable=True)
    likes = relationship("User", secondary=news_users_liked_assoc_association_table, back_populates="liked_news")
    dislikes = relationship("User", secondary=news_users_disliked_assoc_association_table, back_populates="disliked_news")
    interested_in = relationship("User", secondary=news_users_interested_in_assoc_association_table, back_populates="interested_in_news")

    create_date = Column(DateTime, default=datetime.datetime.utcnow)

    clubs = relationship("Club", secondary=news_clubs_assoc_association_table, back_populates="news")

    user = relationship("User", back_populates="news")
    user_id = Column(Integer, ForeignKey('users.id'))

    reports = relationship("Report", back_populates="news")


class Event(Base, ModelPermissionsBase):
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    text = Column(String, nullable=False)
    address = Column(String, nullable=False)
    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=False)
    image = Column(String, nullable=True)

    likes = relationship("User", secondary=events_users_liked_assoc_association_table, back_populates="liked_events")
    dislikes = relationship("User", secondary=events_users_disliked_assoc_association_table, back_populates="disliked_events")
    interested_in = relationship("User", secondary=events_users_interested_in_assoc_association_table, back_populates="interested_in_events")

    create_date = Column(DateTime, default=datetime.datetime.utcnow)

    clubs = relationship("Club", secondary=events_clubs_assoc_association_table, back_populates="events")

    user = relationship("User", back_populates="events")
    user_id = Column(Integer, ForeignKey('users.id'))

    reports = relationship("Report", back_populates="event")


class Offer(Base, ModelPermissionsBase):
    __tablename__ = 'offers'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    text = Column(String, nullable=False)
    address = Column(String, nullable=True)
    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=False)
    image = Column(String, nullable=True)

    likes = relationship("User", secondary=offers_users_liked_assoc_association_table, back_populates="liked_offers")
    dislikes = relationship("User", secondary=offers_users_disliked_assoc_association_table, back_populates="disliked_offers")
    interested_in = relationship("User", secondary=offers_users_interested_in_assoc_association_table, back_populates="interested_in_offers")

    create_date = Column(DateTime, default=datetime.datetime.utcnow)

    clubs = relationship("Club", secondary=offers_clubs_assoc_association_table, back_populates="offers")

    user = relationship("User", back_populates="offers")
    user_id = Column(Integer, ForeignKey('users.id'))

    reports = relationship("Report", back_populates="offer")


class Report(Base):
    __tablename__ = 'reports'

    id = Column(Integer, primary_key=True)
    text = Column(String, nullable=False)

    news_id = Column(Integer, ForeignKey('news.id'))
    news = relationship("News", back_populates="reports")

    event_id = Column(Integer, ForeignKey('events.id'))
    event = relationship("Event", back_populates="reports")

    offer_id = Column(Integer, ForeignKey('offers.id'))
    offer = relationship("Offer", back_populates="reports")

    create_date = Column(DateTime, default=datetime.datetime.utcnow)

    user = relationship("User", back_populates="reports")
    user_id = Column(Integer, ForeignKey('users.id'))


"""=====================SESSIONS======================"""

DBSession = database.create_session("DBSession")
BlockSession = database.create_session("BlockSession")
NewsDigestSession = database.create_session("NewsDigestSession")
EventsDigestSession = database.create_session("EventsDigestSession")
OffersDigestSession = database.create_session("OffersDigestSession")
