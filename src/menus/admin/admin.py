import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import group_buttons, prepare_user
from botmanlib.menus.ready_to_use.permissions import PermissionsMenu
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message
from sqlalchemy import func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, Filters, MessageHandler, CallbackQueryHandler, PrefixHandler

from src.menus.admin.club_requests import ClubRequestsMenu
from src.menus.admin.clubs import ClubsMenu
from src.menus.admin.distributions import DistributionsMenu
from src.menus.admin.settings import SettingsMenu
from src.menus.admin.statistics import StatisticsMenu
from src.models import User, Permission, Club, DBSession, JoinClubRequest


class AdminMenu(BaseMenu):
    menu_name = 'admin_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        user = prepare_user(User, update, context, 'uk')
        _ = context.user_data['user'].translator

        if not user.has_permission('admin_menu_access'):
            delete_user_message(update)
            return ConversationHandler.END

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        delete_interface(context)
        self.send_message(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        club_requests = DBSession.query(func.count(JoinClubRequest.id)).filter(JoinClubRequest.type == None).scalar()
        buttons = []

        if user.has_permission('permissions_menu_access'):
            buttons.append(InlineKeyboardButton(_("Permissions"), callback_data='permissions'))
        if user.has_permission('distribution_menu_access'):
            buttons.append(InlineKeyboardButton(_("Distribution"), callback_data='distribution'))
        if user.has_permission(Club.view_permission):
            buttons.append(InlineKeyboardButton(_("Clubs"), callback_data='clubs'))
        if user.has_permission(JoinClubRequest.view_permission):
            buttons.append(InlineKeyboardButton(_("Club requests") + f" ({club_requests})", callback_data='club_requests'))
        if user.has_permission("settings_menu_access"):
            buttons.append(InlineKeyboardButton(_("Settings"), callback_data='settings'))
        if user.has_permission("statistics_menu_access"):
            buttons.append(InlineKeyboardButton(_("Statistics"), callback_data='statistics'))

        buttons.append(InlineKeyboardButton(_("Back to main menu"), callback_data='start'))

        send_or_edit(context, chat_id=user.chat_id, text=_("Admin menu:"), reply_markup=InlineKeyboardMarkup(group_buttons(buttons, 1)))

    def goto_next_menu(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        permissions_menu = PermissionsMenu(User, Permission, parent=self)
        distribution_menu = DistributionsMenu(self)
        clubs_menu = ClubsMenu(self)
        club_requests = ClubRequestsMenu(self)
        settings_menu = SettingsMenu(self)
        statistics_menu = StatisticsMenu(self)

        handler = ConversationHandler(entry_points=[PrefixHandler('/', "admin", self.entry),
                                                    CallbackQueryHandler(self.entry, pattern='^admin$')],
                                      states={
                                          self.States.ACTION: [distribution_menu.handler,
                                                               CallbackQueryHandler(self.entry, pattern='^back$'),
                                                               permissions_menu.handler,
                                                               clubs_menu.handler,
                                                               club_requests.handler,
                                                               settings_menu.handler,
                                                               statistics_menu.handler,
                                                               ],

                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.regex('^/start$'), self.goto_next_menu),
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
