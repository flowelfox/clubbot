from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, require_permission
from botmanlib.models import MessageType
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.models import DBSession, Club, RegionTranslation
from src.settings import WEBHOOK_ENABLE


class ClubsMenu(OneListMenu):
    menu_name = 'admin_clubs_menu'

    @require_permission(Club.view_permission)
    def entry(self, update, context):
        return super(ClubsMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(Club).order_by(Club.id.asc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^clubs$')]

    def message_text(self, context, club):
        user = context.user_data['user']
        _ = user.translator

        if club:
            message_text = ""
            if club.image is not None:
                message_text = f'<a href="{self.bot.get_image_url(club.image)}">\u200B</a>' if club.image and WEBHOOK_ENABLE else ""

            message_text += f"____{_('Clubs: ')}____\n"
            message_text += _("Title") + f": {club.title}\n"
            message_text += _("Region") + f": {club.region.get_translation(user.language_code).name}\n"
            message_text += _("Link") + f": {club.link}\n"

        else:
            message_text = _("There is no clubs yet") + '\n'

        return message_text

    @require_permission(Club.delete_permission)
    def delete_ask(self, update, context):
        return super(ClubsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        if user.has_permission(Club.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_club"))
        if o:
            if user.has_permission(Club.edit_permission):
                buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_club"))

        return buttons

    def object_buttons(self, context, club):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if club:
            if user.has_permission(Club.delete_permission):
                buttons.append([InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}")])

        return buttons

    def additional_states(self):
        add_edit_club = ClubAddEditMenu(self)
        return {self.States.ACTION: [add_edit_club.handler,
                                     CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Category deleted")


class ClubAddEditMenu(ArrowAddEditMenu):
    menu_name = 'club_add_menu'
    model = Club

    @require_permission(Club.add_permission)
    def entry_add(self, update, context):
        return super(ClubAddEditMenu, self).entry(update, context)

    @require_permission(Club.edit_permission)
    def entry_edit(self, update, context):
        return super(ClubAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        if self.action(context) is ClubAddEditMenu.Action.EDIT:
            return self.parent.selected_object(context)
        else:
            return None

    def load(self, context):
        user = context.user_data['user']
        _ = user.translator

        super(ClubAddEditMenu, self).load(context)

        if self.action(context) is ClubAddEditMenu.Action.EDIT:
            club = self.query_object(context)
            context.user_data[self.menu_name]['region_name'] = club.region.get_translation(user.language_code).name if club.region else None

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_regions = [region for region, in DBSession.query(RegionTranslation.name).filter(RegionTranslation.lang == 'uk').all()]

        fields = [self.Field('title', _("*Title"), validators.String(), required=True),
                  self.Field('region_name', _("*Region"), validator=validators.String(), required=True, variants=all_regions),
                  self.Field('link', _("Chat link"), validators.URL(), required=False),
                  self.Field('image', _("Image"), validators.String(), required=False, message_type=MessageType.photo)]

        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry_add, pattern="^add_club$"),
                CallbackQueryHandler(self.entry_edit, pattern="^edit_club$")]

    def save_object(self, club, context, session=None):

        region_translation = DBSession.query(RegionTranslation).filter(RegionTranslation.name == context.user_data[self.menu_name]['region_name']).first()
        club.region = region_translation.region

        if not add_to_db(club, session):
            return self.conv_fallback(context)