import enum

from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, remove_from_db, group_buttons, require_permission
from botmanlib.messages import send_or_edit
from botmanlib.models import MessageType
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, TelegramError
from telegram.ext import CallbackQueryHandler

from src.models import DBSession, JoinClubRequest, ParticipantType


class ClubRequestsMenu(OneListMenu):
    menu_name = 'club_requests_menu'

    class States(enum.Enum):
        ACTION = 1
        ASSIGNMENT = 2

    @require_permission(JoinClubRequest.view_permission)
    def entry(self, update, context):
        self._load(context)
        context.user_data[self.menu_name]['show_processed'] = False
        return super(ClubRequestsMenu, self).entry(update, context)

    def query_objects(self, context):
        query = DBSession.query(JoinClubRequest)
        if context.user_data[self.menu_name]['show_processed']:
            return query.filter(JoinClubRequest.type != None).all()
        else:
            return query.filter(JoinClubRequest.type == None).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^club_requests$")]

    def message_text(self, context, club_request):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if club_request:
            message_text += _("User") + f': <a href="tg://user?id={club_request.user.chat_id}">{club_request.user.get_name()}</a>' + '\n'
            message_text += _("EDRPOU") + f": {club_request.edrpou}\n"
            message_text += _("Company") + f": {club_request.company}\n"
            message_text += _("City") + f": {club_request.city}\n"
            message_text += _("Owner") + f": {club_request.owner}\n"
            message_text += _("Phone") + f": {club_request.phone}\n"
            message_text += _("Name") + f": {club_request.name}\n"
            if club_request.email:
                message_text += _("Email") + f": {club_request.email}\n"

            message_text += _("Assignment") + ": " + (_("Loaded") if club_request.assignment else _("Not loaded")) + "\n"
            if club_request.club:
                message_text += _("Club:") + f" {club_request.club.title}\n"
                message_text += _("Club region:") + f" {club_request.club.region.get_translation(user.language_code).name}\n"
            else:
                message_text += _("No club selected") + "\n"

            message_text += "\n"
            if club_request.type is None:
                message_text += _("Not processed") + "\n"
            else:
                message_text += _("Processed") + "\n"
        else:
            message_text = _("There is no club requests yet") + '\n'
        return message_text

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator

        buttons = []

        if context.user_data[self.menu_name]['show_processed']:
            buttons.append(InlineKeyboardButton(_("Show unprocessed"), callback_data="show_unprocessed"))
        else:
            buttons.append(InlineKeyboardButton(_("Show processed"), callback_data="show_processed"))
        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        buttons = []
        if obj:
            if user.has_permission(JoinClubRequest.edit_permission):
                buttons.append([InlineKeyboardButton(_("Edit"), callback_data="edit_request")])
            if obj.assignment:
                buttons.append([InlineKeyboardButton(_("Assignment"), callback_data="show_assignment")])

            if obj.type is None:
                buttons.append([InlineKeyboardButton(_("Accept"), callback_data="accept_request"),
                                InlineKeyboardButton(_("Decline"), callback_data="decline_request")])

        return buttons

    def toggle_filter(self, update, context):
        data = update.callback_query.data

        if data == "show_unprocessed":
            context.user_data[self.menu_name]['show_processed'] = False
        else:
            context.user_data[self.menu_name]['show_processed'] = True

        self.update_objects(context)
        update.callback_query.answer()
        self.send_message(context)
        return self.States.ACTION

    def process_payment(self, update, context):
        _ = context.user_data['user'].translator
        data = update.callback_query.data
        request = self.selected_object(context)

        if data == "accept_request":
            self.ask_type(update, context)
        else:
            remove_from_db(request)
            buttons = [[InlineKeyboardButton(_("Close"), callback_data='close_message')]]
            send_or_edit(context, dispatcher=self.dispatcher, user_id=request.user.chat_id, chat_id=request.user.chat_id, interface_name='request_notification', text=_("Your club join request was declined"), reply_markup=InlineKeyboardMarkup(buttons))

            self.update_objects(context)
            update.callback_query.answer()
            self.send_message(context)
        return self.States.ACTION

    def ask_type(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("Please select type to set for this user in club")

        flat_buttons = [InlineKeyboardButton(t.to_str(user.language_code), callback_data=f"set_{t.value}") for t in ParticipantType]
        buttons = group_buttons(flat_buttons)
        buttons.append([InlineKeyboardButton(_("🔙 Back"), callback_data="back_to_menu")])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_type(self, update, context):
        type = update.callback_query.data.replace("set_", "")
        request = self.selected_object(context)
        request.type = ParticipantType[type]

        if not add_to_db(request):
            return self.conv_fallback(context)

        _ = request.user.translator
        buttons = [[InlineKeyboardButton(_("Close"), callback_data='close_message')]]
        send_or_edit(context, dispatcher=self.dispatcher, user_id=request.user.chat_id, chat_id=request.user.chat_id, interface_name='request_notification', text=_("Your club join request was accepted."), reply_markup=InlineKeyboardMarkup(buttons))

        self.update_objects(context)
        update.callback_query.answer()
        self.send_message(context)

        return self.States.ACTION

    def show_assignment(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        request = self.selected_object(context)
        buttons = [[InlineKeyboardButton(_("🔙 Back"), callback_data="back_to_menu")]]

        try:
            send_or_edit(context, chat_id=user.chat_id, photo=request.assignment, reply_markup=InlineKeyboardMarkup(buttons))
        except TelegramError:
            send_or_edit(context, chat_id=user.chat_id, text=_("Can't display assignment"), reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.ASSIGNMENT

    def additional_states(self):
        edit_request_menu = EditRequest(self)
        return {self.States.ACTION: [edit_request_menu.handler,
                                     CallbackQueryHandler(self.toggle_filter, pattern=r"^show_(processed|unprocessed)$"),
                                     CallbackQueryHandler(self.process_payment, pattern=r"^(accept|decline)_request"),
                                     CallbackQueryHandler(self.show_assignment, pattern=r"^show_assignment$"),
                                     CallbackQueryHandler(self.set_type, pattern=r"^set_\w+$"),
                                     CallbackQueryHandler(self.back_to_menu, pattern=r"^back_to_menu$")],
                self.States.ASSIGNMENT: [CallbackQueryHandler(self.back_to_menu, pattern=r"^back_to_menu$")]}


class EditRequest(ArrowAddEditMenu):
    menu_name = "edit_request_menu"
    show_field_selectors = False

    @require_permission(JoinClubRequest.edit_permission)
    def entry(self, update, context):
        return super(EditRequest, self).entry(update, context)

    def query_object(self, context):
        return self.parent.selected_object(context)

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        return [self.Field('edrpou', _("*EDRPOU"), validator=validators.String(), required=True),
                self.Field('company', _("*Company"), validator=validators.String(), required=True),
                self.Field('city', _("*City"), validator=validators.String(), required=True),
                self.Field('owner', _("*Owner"), validator=validators.String(), required=True),
                self.Field('assignment', _("*Assignment"), validator=validators.String(), required=True, message_type=MessageType.document)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^edit_request$")]
