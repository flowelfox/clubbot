import datetime

from botmanlib.menus import ArrowAddEditMenu
from botmanlib.menus.helpers import require_permission, get_settings, write_settings, translator
from formencode import validators
from telegram.ext import CallbackQueryHandler

from src.jobs import stop_news_digest_job, start_news_digest_job, stop_events_digest_job, start_events_digest_job, start_offers_digest_job
from src.settings import SETTINGS_FILE


def convert_day(text, lang):
    _ = translator(lang)
    if text == _("Monday"):
        return 0
    elif text == _("Tuesday"):
        return 1
    elif text == _("Wednesday"):
        return 2
    elif text == _("Thursday"):
        return 3
    elif text == _("Friday"):
        return 4
    elif text == _("Saturday"):
        return 5
    elif text == _("Sunday"):
        return 6


def reverse_convert_day(day, lang):
    _ = translator(lang)
    if day == 0:
        return _("Monday")
    elif day == 1:
        return _("Tuesday")
    elif day == 2:
        return _("Wednesday")
    elif day == 3:
        return _("Thursday")
    elif day == 4:
        return _("Friday")
    elif day == 5:
        return _("Saturday")
    elif day == 6:
        return _("Sunday")


class SettingsMenu(ArrowAddEditMenu):
    menu_name = 'settings_menu'
    model = dict
    force_action = ArrowAddEditMenu.Action.EDIT
    field_buttons_group_size = 2
    disable_web_page_preview = True

    @require_permission('settings_menu_access')
    def entry(self, update, context):
        return super(SettingsMenu, self).entry(update, context)

    def reset_button(self, context):
        return None

    def query_object(self, context):
        settings = get_settings(SETTINGS_FILE)
        return settings.get('settings', {})

    def load(self, context):
        user = context.user_data['user']

        self._load(context)
        settings = get_settings(SETTINGS_FILE)
        context.user_data[self.menu_name]["news_digest_day"] = reverse_convert_day(settings['news_digest']['day'], user.language_code)
        context.user_data[self.menu_name]["events_digest_day"] = reverse_convert_day(settings['events_digest']['day'], user.language_code)
        context.user_data[self.menu_name]["offers_digest_day"] = reverse_convert_day(settings['offers_digest']['day'], user.language_code)

        context.user_data[self.menu_name]["news_digest_time"] = datetime.time(hour=settings['news_digest']['hour'], minute=settings['news_digest']['minute'])
        context.user_data[self.menu_name]["events_digest_time"] = datetime.time(hour=settings['events_digest']['hour'], minute=settings['events_digest']['minute'])
        context.user_data[self.menu_name]["offers_digest_time"] = datetime.time(hour=settings['offers_digest']['hour'], minute=settings['offers_digest']['minute'])

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('news_digest_day', _("*News digest day"), validators.String(), required=True, default=_("Monday"), variants=[_("Monday"), _("Tuesday"), _("Wednesday"), _("Thursday"), _("Friday"), _("Saturday"), _("Sunday")]),
                  self.Field('news_digest_time', _("*News digest time"), validators.TimeConverter(use_datetime=True), required=True),

                  self.Field('events_digest_day', _("*Events digest day"), validators.String(), required=True, default=_("Monday"), variants=[_("Monday"), _("Tuesday"), _("Wednesday"), _("Thursday"), _("Friday"), _("Saturday"), _("Sunday")]),
                  self.Field('events_digest_time', _("*Events digest time"), validators.TimeConverter(use_datetime=True), required=True),

                  self.Field('offers_digest_day', _("*Offers digest day"), validators.String(), required=True, default=_("Monday"), variants=[_("Monday"), _("Tuesday"), _("Wednesday"), _("Thursday"), _("Friday"), _("Saturday"), _("Sunday")]),
                  self.Field('offers_digest_time', _("*Offers digest time"), validators.TimeConverter(use_datetime=True), required=True)
                  ]

        return fields

    def save_object(self, obj, context, session=None):
        user = context.user_data['user']

        news_digest = {"day": convert_day(context.user_data[self.menu_name]["news_digest_day"], user.language_code),
                       "hour": context.user_data[self.menu_name]["news_digest_time"].hour,
                       "minute": context.user_data[self.menu_name]["news_digest_time"].minute}

        events_digest = {"day": convert_day(context.user_data[self.menu_name]["events_digest_day"], user.language_code),
                         "hour": context.user_data[self.menu_name]["events_digest_time"].hour,
                         "minute": context.user_data[self.menu_name]["events_digest_time"].minute}

        offers_digest = {"day": convert_day(context.user_data[self.menu_name]["offers_digest_day"], user.language_code),
                         "hour": context.user_data[self.menu_name]["offers_digest_time"].hour,
                         "minute": context.user_data[self.menu_name]["offers_digest_time"].minute}

        write_settings({'news_digest': news_digest}, SETTINGS_FILE)
        write_settings({'events_digest': events_digest}, SETTINGS_FILE)
        write_settings({'offers_digest': offers_digest}, SETTINGS_FILE)

        self.logger.info("Settings updated...")
        start_news_digest_job(context.job_queue)
        start_events_digest_job(context.job_queue)
        start_offers_digest_job(context.job_queue)

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^settings$")]
