from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, unknown_command
from botmanlib.menus.ready_to_use.instant_distribution import InstantDistributionMenu
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.models import User


class DistributionsMenu(BaseMenu):
    menu_name = 'distribution_menu'

    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if not user.has_permission('distribution_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return self.States.ACTION

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton(_("Instant distribution"), callback_data='instant_distribution')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Choose distribution type:"), reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        instant_distribution_menu = ClubInstantDistributionMenu(User, self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^distribution$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               instant_distribution_menu.handler,
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler


class ClubInstantDistributionMenu(InstantDistributionMenu):
    menu_name = 'instant_distribution'

    def available_buttons(self, context):
        _ = context.user_data['user'].translator
        buttons = [{"data": "start", "name": _("Return user to start menu")}, ]
        return buttons
