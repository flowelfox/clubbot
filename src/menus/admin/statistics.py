from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, unknown_command
from botmanlib.menus.ready_to_use.instant_distribution import InstantDistributionMenu
from botmanlib.messages import send_or_edit
from sqlalchemy import func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.models import User, DBSession, Region, RegionTranslation


class StatisticsMenu(BaseMenu):
    menu_name = 'statistics_menu'

    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if not user.has_permission('statistics_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return self.States.ACTION

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_users = DBSession.query(func.count(User.id)).scalar()
        blocked_users = DBSession.query(func.count(User.id)).filter(User.is_active == False).scalar()
        active_users = DBSession.query(func.count(User.id)).filter(User.is_active == True).scalar()
        regions = DBSession.query(Region).join(Region.translations).order_by(RegionTranslation.name).all()

        message_text = "<b>" + _("📊 Bot statistics") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        message_text += _("Bot users") + f": {all_users}\n"
        message_text += _("Active users") + f": {active_users}\n"
        message_text += _("Inactive users") + f": {blocked_users}\n"
        message_text += _("Users per region:") + "\n"
        for region in regions:
            region_users_active = DBSession.query(func.count(User.id)).filter(User.region == region).filter(User.is_active == True).scalar()
            region_users_all = DBSession.query(func.count(User.id)).filter(User.region == region).scalar()

            message_text += f"🔹 {region.get_translation(user.language_code).name}: {region_users_active}({region_users_all}) | {region_users_active / active_users * 100:.2f}%\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^statistics$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler


class ClubInstantDistributionMenu(InstantDistributionMenu):
    menu_name = 'instant_distribution'

    def available_buttons(self, context):
        _ = context.user_data['user'].translator
        buttons = [{"data": "start", "name": _("Return user to start menu")}, ]
        return buttons
