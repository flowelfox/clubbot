import enum

from botmanlib.menus import ArrowAddEditMenu
from botmanlib.menus.helpers import generate_regex_handlers, require_permission, add_to_db
from botmanlib.messages import delete_interface
from formencode import validators
from telegram import InlineKeyboardButton, TelegramError
from telegram.ext import ConversationHandler, CallbackQueryHandler

from src.models import DBSession, User, RegionTranslation


class ProfileMenu(ArrowAddEditMenu):
    class States(enum.Enum):
        ACTION = 1

    menu_name = 'profile_menu'
    model = User
    show_reset_field = True
    reset_to_default = True
    use_html_text = True
    force_action = ArrowAddEditMenu.Action.EDIT
    variants_per_page = 10
    resend_after_each_field = True
    show_field_selectors = False

    @require_permission('profile_menu_access')
    def entry(self, update, context):
        return super(ProfileMenu, self).entry(update, context)

    def load(self, context):
        user = context.user_data['user']
        _ = user.translator

        super(ProfileMenu, self).load(context)

        context.user_data[self.menu_name]['region_name'] = user.region.get_translation(user.language_code).name if user.region else None

        if not context.user_data[self.menu_name]['desired_name']:
            context.user_data[self.menu_name]['desired_name'] = user.name

    def query_object(self, context):
        return DBSession.query(User).get(context.user_data['user'].id)

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_regions = [region for region, in DBSession.query(RegionTranslation.name).filter(RegionTranslation.lang == 'uk').all()]

        return [self.Field('desired_name', _("1.Full name"), validator=validators.String(), required=True),
                self.Field('region_name', _("2.Region"), validator=validators.String(), required=True, variants=all_regions),
                ]

    def variant_center_buttons(self, context):
        return []

    def entry_points(self):
        return generate_regex_handlers("My profile", self.entry, left_text='📋') + \
               [CallbackQueryHandler(self.entry, pattern='profile')]

    def message_top_text(self, context):
        _ = context.user_data['user'].translator
        active_field = self.active_field(context)

        message_text = ""
        if active_field.param == 'desired_name':
            message_text += _("✍ <b>Please enter your full name and send to me</b>") + "\n"
        if active_field.param == 'phone':
            message_text += _("✍ <b>Please enter your phone number in the XXXXXXXXXXXX format and send it to me</b>") + "\n"

        message_text += "\n"
        message_text += "<b>" + _("Your personal info:") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        return message_text

    def save_button(self, context):
        return None

    def bottom_buttons(self, context):
        _ = context.user_data['user'].translator
        menu_data = context.user_data[self.menu_name]
        if menu_data['desired_name'] and menu_data['region_name']:
            return [[InlineKeyboardButton(_("✅ Save"), callback_data=f"save_{self.menu_name}")]]

    def reset_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("🧹 Reset field"), callback_data="reset_field")

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("🏠 Main menu"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def save(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if not self.check_fields(context):
            delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        delete_interface(context, 'variations_interface')

        self.update_object(context)
        try:
            self.bot.answer_callback_query(update.callback_query.id, text=_("Your information saved."), show_alert=True)
        except TelegramError:
            self.bot.send_message(chat_id=user.chat_id, text=_("Your information saved."))

        return self.back(update, context)

    def save_object(self, obj, context, session=None):
        user_data = context.user_data
        _ = user_data['_']

        region_translation = DBSession.query(RegionTranslation).filter(RegionTranslation.name == user_data[self.menu_name]['region_name']).first()
        obj.region = region_translation.region

        add_to_db(obj)
