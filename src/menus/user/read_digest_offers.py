import datetime
import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db, prepare_user
from botmanlib.messages import send_or_edit, delete_user_message, remove_interface, delete_interface
from formencode import validators, Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, JoinClubRequest, Club, Offer, Report, User
from src.settings import WEBHOOK_ENABLE


class ReadOffersMenu(OneListMenu):
    menu_name = 'read_offers_menu'
    disable_web_page_preview = False

    class States(enum.Enum):
        ACTION = 1
        REPORT = 2

    def entry(self, update, context):
        user = prepare_user(User, update, context, lang='uk')
        self._load(context)

        date_str = update.callback_query.data.replace("read_offers_", "")
        context.user_data[self.menu_name]['date'] = datetime.datetime.strptime(date_str, "%d.%m.%Y")
        update.effective_message.delete()
        delete_interface(context)
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']
        regions = [user.region_id] + [club.region_id for club in user.clubs]
        offers = DBSession.query(Offer) \
            .join(Offer.clubs) \
            .filter(Offer.create_date > datetime.datetime.utcnow() - datetime.timedelta(days=7)) \
            .filter(Club.region_id.in_(regions)) \
            .all()

        return offers

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^read_offers_\d+\.\d+.\d+$')]

    def message_text(self, context, offer):
        user = context.user_data['user']
        _ = user.translator

        if offer:
            message_text = ""
            if offer.image:
                message_text = f'<a href="{self.bot.get_image_url(offer.image)}">\u200B</a>' if offer.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + offer.title + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("⏱️ Start:") + "</b> " + offer.start_date.strftime("%H:%M %d.%m.%Y") + "\n"
            message_text += "<b>" + _("⏱️ End:") + "</b> " + offer.end_date.strftime("%H:%M %d.%m.%Y") + "\n"
            message_text += "<b>" + _("📍️ Address:") + "</b> " + offer.address + "\n\n"

            if len(offer.text) > 100:
                message_text += offer.text[:100] + "..."
            else:
                message_text += offer.text
            message_text += "\n"

        else:
            message_text = _("There is no offers yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator
        clubs = user.clubs

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        if len(clubs) > 1:
            message_text += "<i>" + _("🔖️ Offers of all your regions") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"
        else:
            message_text += "<i>" + _("🔖️ Offers of {region}").format(region=user.region.get_translation(user.language_code).name_declension) + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"

        return message_text

    def object_buttons(self, context, offer):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if offer:
            buttons.append([InlineKeyboardButton(_("👍 {likes}").format(likes=len(offer.likes)), callback_data="like_offer"),
                            InlineKeyboardButton(_("⭐ Interested in {interested_in}").format(interested_in=len(offer.interested_in)), callback_data="interested_in_offer"),
                            InlineKeyboardButton(_("👎 {dislikes}").format(dislikes=len(offer.dislikes)), callback_data="dislike_offer")])

            buttons.append([InlineKeyboardButton(_("👀 More"), callback_data='more')])

        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("🏠 Main menu"), callback_data="start")

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        offer = self.selected_object(context)
        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == offer.user) \
            .filter(JoinClubRequest.club_id.in_([c.id for c in offer.clubs])) \
            .first()

        message_text = "<b>" + offer.title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += offer.text + "\n\n"
        author_link = f'<a href="{offer.user.mention_url}">{offer.user.get_name()}</a>' if user.clubs else offer.user.get_name()
        message_text += "<b>" + _("© Author:") + "</b> " + f'{author_link} ({request.company})\n\n'

        buttons = [[InlineKeyboardButton(_("🚨 Report"), callback_data='report_post')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def ask_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your report text and send it to me") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.REPORT

    def set_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            text = validators.String().to_python(update.effective_message.text)

            report = Report()
            report.text = text
            report.user = user
            report.offer = self.selected_object(context)
            add_to_db(report)

            delete_user_message(update)

            message_text = "<b>" + _("✅ Your report was sent") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

            return self.States.REPORT
        except Invalid:
            remove_interface(context, 'wrong_text')
            send_or_edit(context, "wrong_text", chat_id=user.chat_id, text=_("Wrong text, please try again"))
            delete_interface(context)
            return self.ask_report(update, context)

    def like(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        offer = self.selected_object(context)
        if user in offer.likes:
            offer.likes.remove(user)
        else:
            offer.likes.append(user)

        if user in offer.dislikes:
            offer.dislikes.remove(user)

        if user in offer.interested_in:
            offer.interested_in.remove(user)

        add_to_db(offer, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def dislike(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        offer = self.selected_object(context)
        if user in offer.dislikes:
            offer.dislikes.remove(user)
        else:
            offer.dislikes.append(user)

        if user in offer.likes:
            offer.likes.remove(user)

        if user in offer.interested_in:
            offer.interested_in.remove(user)

        add_to_db(offer, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def interested_in(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        offer = self.selected_object(context)
        if user in offer.interested_in:
            offer.interested_in.remove(user)
        else:
            offer.interested_in.append(user)

        if user in offer.likes:
            offer.likes.remove(user)

        if user in offer.dislikes:
            offer.dislikes.remove(user)

        add_to_db(offer, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.more, pattern="^more$"),
                                     CallbackQueryHandler(self.like, pattern="^like_offer$"),
                                     CallbackQueryHandler(self.dislike, pattern="^dislike_offer$"),
                                     CallbackQueryHandler(self.interested_in, pattern="^interested_in_offer$"),
                                     CallbackQueryHandler(self.ask_report, pattern="^report_post$")],
                self.States.REPORT: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     MessageHandler(Filters.text, self.set_report)]}
