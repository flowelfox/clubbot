import datetime
import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db, prepare_user
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface, remove_interface
from formencode import validators, Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, JoinClubRequest, Club, Event, Report, User
from src.settings import WEBHOOK_ENABLE


class ReadEventsMenu(OneListMenu):
    menu_name = 'read_events_menu'
    disable_web_page_preview = False

    class States(enum.Enum):
        ACTION = 1
        REPORT = 2

    def entry(self, update, context):
        user = prepare_user(User, update, context, lang='uk')
        self._load(context)

        date_str = update.callback_query.data.replace("read_events_", "")
        context.user_data[self.menu_name]['date'] = datetime.datetime.strptime(date_str, "%d.%m.%Y")
        update.effective_message.delete()
        delete_interface(context)
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']

        regions = [user.region_id] + [club.region_id for club in user.clubs]
        events = DBSession.query(Event) \
            .join(Event.clubs) \
            .filter(Event.start_date < datetime.datetime.utcnow() + datetime.timedelta(days=21)) \
            .filter(Club.region_id.in_(regions)) \
            .all()

        return events

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^read_events_\d+\.\d+.\d+$')]

    def message_text(self, context, event):
        user = context.user_data['user']
        _ = user.translator

        if event:
            message_text = ""
            if event.image:
                message_text = f'<a href="{self.bot.get_image_url(event.image)}">\u200B</a>' if event.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + event.title + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("⏱️ Start:") + "</b> " + event.start_date.strftime("%H:%M %d.%m.%Y") + "\n"
            message_text += "<b>" + _("⏱️ End:") + "</b> " + event.end_date.strftime("%H:%M %d.%m.%Y") + "\n"
            message_text += "<b>" + _("📍️ Address:") + "</b> " + event.address + "\n\n"

            if len(event.text) > 100:
                message_text += event.text[:100] + "..."
            else:
                message_text += event.text
            message_text += "\n"
        else:
            message_text = _("There is no events yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator
        clubs = user.clubs

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        if len(clubs) > 1:
            message_text += "<i>" + _("🗓️ Events of all your regions") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"
        else:
            message_text += "<i>" + _("🗓️ Events of {region}").format(region=user.region.get_translation(user.language_code).name_declension) + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"

        return message_text

    def object_buttons(self, context, event):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if event:
            buttons.append([InlineKeyboardButton(_("✅ Going {likes}").format(likes=len(event.likes)), callback_data="like_event"),
                            InlineKeyboardButton(_("⭐ Interested in {interested_in}").format(interested_in=len(event.interested_in)), callback_data="interested_in_event"),
                            InlineKeyboardButton(_("🚷 Not going {dislikes}").format(dislikes=len(event.dislikes)), callback_data="dislike_event")])
            buttons.append([InlineKeyboardButton(_("👀 More"), callback_data='more')])

        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("🏠 Main menu"), callback_data="start")

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        event = self.selected_object(context)
        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == event.user) \
            .filter(JoinClubRequest.club_id.in_([c.id for c in event.clubs])) \
            .first()

        message_text = "<b>" + event.title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += event.text + "\n\n"
        author_link = f'<a href="{event.user.mention_url}">{event.user.get_name()}</a>' if user.clubs else event.user.get_name()
        message_text += "<b>" + _("© Author:") + "</b> " + f'{author_link} ({request.company})\n\n'

        buttons = [[InlineKeyboardButton(_("🚨 Report"), callback_data='report_post')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def ask_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your report text and send it to me") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.REPORT

    def set_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            text = validators.String().to_python(update.effective_message.text)

            report = Report()
            report.text = text
            report.user = user
            report.event = self.selected_object(context)
            add_to_db(report)

            delete_user_message(update)

            message_text = "<b>" + _("✅ Your report was sent") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

            return self.States.REPORT
        except Invalid:
            remove_interface(context, 'wrong_text')
            send_or_edit(context, "wrong_text", chat_id=user.chat_id, text=_("Wrong text, please try again"))
            delete_interface(context)
            return self.ask_report(update, context)

    def like(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        event = self.selected_object(context)
        if user in event.likes:
            event.likes.remove(user)
        else:
            event.likes.append(user)

        if user in event.dislikes:
            event.dislikes.remove(user)

        if user in event.interested_in:
            event.interested_in.remove(user)

        add_to_db(event, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def dislike(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        event = self.selected_object(context)
        if user in event.dislikes:
            event.dislikes.remove(user)
        else:
            event.dislikes.append(user)

        if user in event.likes:
            event.likes.remove(user)

        if user in event.interested_in:
            event.interested_in.remove(user)

        add_to_db(event, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def interested_in(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        event = self.selected_object(context)
        if user in event.interested_in:
            event.interested_in.remove(user)
        else:
            event.interested_in.append(user)

        if user in event.likes:
            event.likes.remove(user)

        if user in event.dislikes:
            event.dislikes.remove(user)

        add_to_db(event, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.more, pattern="^more$"),
                                     CallbackQueryHandler(self.like, pattern="^like_event$"),
                                     CallbackQueryHandler(self.dislike, pattern="^dislike_event$"),
                                     CallbackQueryHandler(self.interested_in, pattern="^interested_in_event$"),
                                     CallbackQueryHandler(self.ask_report, pattern="^report_post$")],
                self.States.REPORT: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     MessageHandler(Filters.text, self.set_report)]}
