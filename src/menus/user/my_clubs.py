from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import inline_placeholder, require_permission
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.models import DBSession, Club, JoinClubRequest
from src.settings import WEBHOOK_ENABLE


class MyClubsMenu(OneListMenu):
    menu_name = 'my_clubs_menu'
    disable_web_page_preview = False

    @require_permission('my_clubs_menu_access')
    def entry(self, update, context):
        return super(MyClubsMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']
        return DBSession.query(Club)\
            .join(Club.requests) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.type != None) \
            .order_by(Club.title.asc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^my_clubs$')]

    def message_text(self, context, club):
        user = context.user_data['user']
        _ = user.translator

        if club:
            request = DBSession.query(JoinClubRequest)\
                .filter(JoinClubRequest.club == club) \
                .filter(JoinClubRequest.user == user) \
                .first()

            message_text = ""
            if club.image is not None:
                message_text = f'<a href="{self.bot.get_image_url(club.image)}">\u200B</a>' if club.image and WEBHOOK_ENABLE else ""

            message_text += "<b>" + _("Best agricultural practices {region}").format(region=club.region.get_translation(user.language_code).name_declension) + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += _("Membership:") + f" {request.type.to_str(user.language_code)}\n"
            message_text += _("Join date:") + f" {request.create_date.strftime('%d.%m.%Y')}\n"
            message_text += "\n"
        else:
            message_text = _("There is no clubs yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("👨‍🌾 My clubs") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def center_buttons(self, context, o=None):
        user = context.user_data['user']
        _ = user.translator

        buttons = []
        if o:
            if o.link:
                buttons.append(InlineKeyboardButton(_("💬 Club chat"), url=o.link))
        return buttons

    def object_buttons(self, context, club):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if club:
            buttons.append([InlineKeyboardButton(_("👥 Club members"), callback_data="club_members")])
            buttons.append([InlineKeyboardButton(_("🙋‍♂️ General meeting"), callback_data="general_meeting")])

        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(inline_placeholder(self.States.ACTION, text="Under construction"), pattern="general_meeting"),
                                     CallbackQueryHandler(inline_placeholder(self.States.ACTION, text="Under construction"), pattern="club_members")]}
