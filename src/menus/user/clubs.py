from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.user.join_club import JoinClubMenu
from src.menus.user.my_clubs import MyClubsMenu
from src.menus.user.on_moderation import OnModerationMenu
from src.menus.user.waiting_assignment import WaitingAssignmentMenu


class ClubsMenu(BaseMenu):
    menu_name = 'user_clubs_menu'

    @require_permission('clubs_menu_access')
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        user = context.user_data['user']
        _ = user.translator

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        clubs_count = len(self.my_clubs_menu.query_objects(context))
        assignment_count = len(self.waiting_assignment_menu.query_objects(context))
        on_moderation_count = len(self.on_moderation_menu.query_objects(context))


        buttons = []

        if user.club_requests:
            message_text = "<b>" + _("👨‍🌾 My clubs") + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += _("For controlling your clubs or requests to participate select appropriate section:")

            buttons.append([InlineKeyboardButton(_("✅ My clubs") + f" ({clubs_count})", callback_data='my_clubs')])
            buttons.append([InlineKeyboardButton(_("⏳ On moderation") + f" ({on_moderation_count})", callback_data='on_moderation')])
            buttons.append([InlineKeyboardButton(_("⚠️ Waiting for assignment") + f" ({assignment_count})", callback_data='waiting_assignment')])
        else:
            message_text = "<b>" + _("👨‍🌾 My clubs") + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("You are not added any club yet. Press <b>➕ Add club</b> to join club 👇")

        buttons.append([InlineKeyboardButton(_("➕ Add club"), callback_data='join_club_menu')])
        buttons.append([InlineKeyboardButton(_("ℹ️ More about clubs"), callback_data='more_about_clubs')])
        buttons.append([InlineKeyboardButton(_("🏠 Main menu"), callback_data=f'back_{self.menu_name}')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def more_about_clubs(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("ℹ Information") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("For getting more additional info about club select appropriate section:")

        buttons = [[InlineKeyboardButton(_("📜 Club regulations"), url="https://agroport.ua/statut")],
                   [InlineKeyboardButton(_("🌎 Site"), url="https://agroport.ua/club")],
                   [InlineKeyboardButton(_("🎞️ Presentations (PDF)"), url="https://aviabrand.bitrix24.ua/~fGIId")],
                   [InlineKeyboardButton(_("💬 ️Online consultant"), url="https://t.me/AgroportUkraine")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.parent.send_message(context)
        if update.callback_query and update.callback_query.id != 0:
            update.callback_query.answer()
        return ConversationHandler.END

    def get_handler(self):
        join_club_menu = JoinClubMenu(self)
        self.waiting_assignment_menu = WaitingAssignmentMenu(self)
        self.my_clubs_menu = MyClubsMenu(self)
        self.on_moderation_menu = OnModerationMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^my_clubs_menu$')],
                                      states={
                                          self.States.ACTION: [join_club_menu.handler,
                                                               self.waiting_assignment_menu.handler,
                                                               self.my_clubs_menu.handler,
                                                               self.on_moderation_menu.handler,
                                                               CallbackQueryHandler(self.more_about_clubs, pattern=f'^more_about_clubs$'),
                                                               CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               CallbackQueryHandler(self.back_to_menu, pattern=f'^back_to_menu_{self.menu_name}$')],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
