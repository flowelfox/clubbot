from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import inline_placeholder, require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.user.announcement import AnnouncementMenu
from src.menus.user.view_events import ViewEventsMenu
from src.menus.user.view_news import ViewNewsMenu
from src.menus.user.view_offers import ViewOffersMenu


class DigestMenu(BaseMenu):
    menu_name = 'digest_menu'

    @require_permission('digest_menu_access')
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        user = context.user_data['user']
        _ = user.translator
        if not user.region:
            update.callback_query.answer(text=_("⚠️ To access the section, register and specify the region you want to receive the digest!\n\nTo register, fill in your details in the 👤 My profile of the main menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        clubs = user.clubs
        news_count = len(self.view_news_menu.query_objects(context))
        events_count = len(self.view_events_menu.query_objects(context))
        offers_count = len(self.view_offers_menu.query_objects(context))

        if len(clubs) > 1:
            message_text = "<b>" + _("📬 Digest of all your regions") + '</b>\n'
        else:
            if clubs:
                region = clubs[0].region.get_translation(user.language_code).name_declension
            else:
                region = user.region.get_translation(user.language_code).name_declension
            message_text = "<b>" + _("📬 Digest of {region_name}").format(region_name=region) + '</b>\n'

        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Select digest section:")

        buttons = [[InlineKeyboardButton(_("🗞️️ News") + f" ({news_count})", callback_data='view_news')],
                   [InlineKeyboardButton(_("🗓️ Events") + f" ({events_count})", callback_data='view_events')],
                   [InlineKeyboardButton(_("🔖️ Special offers") + f" ({offers_count})", callback_data='view_offers')],
                   [InlineKeyboardButton(_("📣 Create announcement"), callback_data='create_announcement')],
                   [InlineKeyboardButton(_("🙋‍♂️️ General meeting"), callback_data='general_meeting')],
                   [InlineKeyboardButton(_("🏠 Main menu"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.parent.send_message(context)
        if update.callback_query and update.callback_query.id != 0:
            update.callback_query.answer()
        return ConversationHandler.END

    def get_handler(self):
        self.view_news_menu = ViewNewsMenu(self)
        self.view_events_menu = ViewEventsMenu(self)
        self.view_offers_menu = ViewOffersMenu(self)
        announcement_menu = AnnouncementMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^digest$')],
                                      states={
                                          self.States.ACTION: [self.view_news_menu.handler,
                                                               self.view_events_menu.handler,
                                                               self.view_offers_menu.handler,
                                                               announcement_menu.handler,
                                                               CallbackQueryHandler(inline_placeholder(self.States.ACTION, text="Under construction"), pattern="general_meeting"),
                                                               CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               CallbackQueryHandler(self.back_to_menu, pattern=f'^back_to_menu$')]
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
