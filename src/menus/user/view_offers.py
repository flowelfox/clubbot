import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db, require_permission
from botmanlib.messages import send_or_edit, delete_user_message, remove_interface, delete_interface
from formencode import validators, Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, JoinClubRequest, Club, Offer, Report
from src.settings import WEBHOOK_ENABLE


class ViewOffersMenu(OneListMenu):
    menu_name = 'view_offers_menu'
    disable_web_page_preview = False

    class States(enum.Enum):
        ACTION = 1
        REPORT = 2

    @require_permission('view_offers_menu_access')
    def entry(self, update, context):
        return super(ViewOffersMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']
        user_clubs = user.clubs
        """
        1. если не в тклубе - то по указанному регионе в профиле
        2. если один клуб - новости по региону из профиля и из клуба
        3. Если несколько клубов - по аналоггии с п.2
        """

        query = DBSession.query(Offer) \
            .join(Offer.clubs)

        if len(user_clubs) == 0:
            query = query.filter(Club.region == user.region)
        elif len(user_clubs) > 0:
            query = query.filter(Club.region_id.in_([user.region_id, user_clubs[0].region_id]))

        return query.order_by(Offer.create_date.desc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^view_offers$')]

    def message_text(self, context, offer):
        user = context.user_data['user']
        _ = user.translator

        if offer:
            message_text = ""
            if offer.image:
                message_text = f'<a href="{self.bot.get_image_url(offer.image)}">\u200B</a>' if offer.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + offer.title + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("⏱️ Start:") + "</b> " + offer.start_date.strftime("%H:%M %d.%m.%Y") + "\n"
            message_text += "<b>" + _("⏱️ End:") + "</b> " + offer.end_date.strftime("%H:%M %d.%m.%Y") + "\n"
            message_text += "<b>" + _("📍️ Address:") + "</b> " + offer.address + "\n\n"

            if len(offer.text) > 100:
                message_text += offer.text[:100] + "..."
            else:
                message_text += offer.text
            message_text += "\n"

        else:
            message_text = _("There is no offers yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator
        clubs = user.clubs

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        if len(clubs) > 1:
            message_text += "<i>" + _("🔖️ Offers of all your regions") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"
        else:
            message_text += "<i>" + _("🔖️ Offers of {region}").format(region=user.region.get_translation(user.language_code).name_declension) + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"

        return message_text

    def object_buttons(self, context, offer):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if offer:
            buttons.append([InlineKeyboardButton(_("👍 {likes}").format(likes=len(offer.likes)), callback_data="like_offer"),
                            InlineKeyboardButton(_("⭐ Interested in {interested_in}").format(interested_in=len(offer.interested_in)), callback_data="interested_in_offer"),
                            InlineKeyboardButton(_("👎 {dislikes}").format(dislikes=len(offer.dislikes)), callback_data="dislike_offer")])

            buttons.append([InlineKeyboardButton(_("👀 More"), callback_data='more')])

        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        offer = self.selected_object(context)
        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == offer.user) \
            .filter(JoinClubRequest.club_id.in_([c.id for c in offer.clubs])) \
            .first()

        message_text = "<b>" + offer.title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += offer.text + "\n\n"
        author_link = f'<a href="{offer.user.mention_url}">{offer.user.get_name()}</a>' if user.clubs else offer.user.get_name()
        message_text += "<b>" + _("© Author:") + "</b> " + f'{author_link} ({request.company})\n\n'

        buttons = [[InlineKeyboardButton(_("🚨 Report"), callback_data='report_post')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def ask_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your report text and send it to me") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.REPORT

    def set_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            text = validators.String().to_python(update.effective_message.text)

            report = Report()
            report.text = text
            report.user = user
            report.offer = self.selected_object(context)
            add_to_db(report)

            delete_user_message(update)

            message_text = "<b>" + _("✅ Your report was sent") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

            return self.States.REPORT
        except Invalid:
            remove_interface(context, 'wrong_text')
            send_or_edit(context, "wrong_text", chat_id=user.chat_id, text=_("Wrong text, please try again"))
            delete_interface(context)
            return self.ask_report(update, context)

    def like(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        offer = self.selected_object(context)
        if user in offer.likes:
            offer.likes.remove(user)
        else:
            offer.likes.append(user)

        if user in offer.dislikes:
            offer.dislikes.remove(user)

        if user in offer.interested_in:
            offer.interested_in.remove(user)

        add_to_db(offer, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def dislike(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        offer = self.selected_object(context)
        if user in offer.dislikes:
            offer.dislikes.remove(user)
        else:
            offer.dislikes.append(user)

        if user in offer.likes:
            offer.likes.remove(user)

        if user in offer.interested_in:
            offer.interested_in.remove(user)

        add_to_db(offer, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def interested_in(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        offer = self.selected_object(context)
        if user in offer.interested_in:
            offer.interested_in.remove(user)
        else:
            offer.interested_in.append(user)

        if user in offer.likes:
            offer.likes.remove(user)

        if user in offer.dislikes:
            offer.dislikes.remove(user)

        add_to_db(offer, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.more, pattern="^more$"),
                                     CallbackQueryHandler(self.like, pattern="^like_offer$"),
                                     CallbackQueryHandler(self.dislike, pattern="^dislike_offer$"),
                                     CallbackQueryHandler(self.interested_in, pattern="^interested_in_offer$"),
                                     CallbackQueryHandler(self.ask_report, pattern="^report_post$")],
                self.States.REPORT: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     MessageHandler(Filters.text, self.set_report)]}
