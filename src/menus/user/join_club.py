import datetime
import enum
import locale
import re
import smtplib
import ssl
from email.encoders import encode_base64
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO
from os import path
from textwrap import wrap

import requests
from PyPDF2 import PdfFileReader, PdfFileWriter
from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db, generate_regex_handlers, require_permission, setlocale
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface, remove_interface, remove_interface_markup
from formencode import Invalid, validators
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters, ConversationHandler

from src.models import DBSession, Club, JoinClubRequest, User, Permission
from src.settings import WEBHOOK_ENABLE, YOUCONTROL_TOKEN, RESOURCES_FOLDER, SENDER_EMAIL, SENDER_SERVER, SENDER_SERVER_PORT, SENDER_PASSWORD, SENDER_USERNAME


class JoinClubMenu(OneListMenu):
    menu_name = 'join_club_menu'
    disable_web_page_preview = False

    class States(enum.Enum):
        ACTION = 1
        PHONE = 2
        NAME = 3
        ERDPOU = 4
        ASSIGNMENT = 5
        EMAIL = 6
        DOWNLOAD_BLANK = 7
        AFTER_EMAIL = 8
        SEND = 9

    @require_permission('join_club_menu_access')
    def entry(self, update, context):
        return super(JoinClubMenu, self).entry(update, context)

    def _load(self, context):
        super(JoinClubMenu, self)._load(context)
        context.user_data[self.menu_name]["edrpou"] = None
        context.user_data[self.menu_name]["name"] = None
        context.user_data[self.menu_name]["owner"] = None
        context.user_data[self.menu_name]["city"] = None
        context.user_data[self.menu_name]["user_name"] = None
        context.user_data[self.menu_name]["phone"] = None
        context.user_data[self.menu_name]["email"] = None

    def query_objects(self, context):
        user = context.user_data['user']
        clubs = DBSession.query(Club).all()
        not_added_clubs = []

        for club in clubs:
            if any([req in user.club_requests for req in club.requests]):
                continue
            not_added_clubs.append(club)
        with setlocale(""):
            return sorted(not_added_clubs, key=lambda x: locale.strxfrm(x.region.get_translation(user.language_code).name))

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^join_club_menu$')]

    def message_text(self, context, club):
        user = context.user_data['user']
        _ = user.translator

        if club:
            message_text = ""
            if club.image is not None:
                message_text = f'<a href="{self.bot.get_image_url(club.image)}">\u200B</a>' if club.image and WEBHOOK_ENABLE else ""

            message_text += "<b>" + _("Best agricultural practices {region}").format(region=club.region.get_translation(user.language_code).name_declension) + '</b>\n'
            # message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        else:
            message_text = _("There is no clubs yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("➕ Add club") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def object_buttons(self, context, club):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if club:
            buttons.append([InlineKeyboardButton(_("📝 Application for membership"), callback_data="join_club")])

        return buttons

    def ask_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📲 Send your phone number to me") + "</b>\n\n"
        message_text += _("Please press button «📲 Send phone number» 👇") + "\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("📝 Application for membership (Step {step} of {of})").format(step=1, of=5)

        buttons = [[KeyboardButton(_("📲 Send phone number"), request_contact=True)],
                   [KeyboardButton(_("⏪ Back"))]]

        delete_interface(context)
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")
        return self.States.PHONE

    def wrong_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("⚠️ Don't need to enter phone number manually, just press please button 📲 Send phone number (It is below text field).") + "\n"

        buttons = [[KeyboardButton(_("📲 Send phone number"), request_contact=True)],
                   [KeyboardButton(_("⏪ Back"))]]

        delete_interface(context)
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")
        return self.States.PHONE

    def set_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        phone = update.message.contact.phone_number
        if not phone:
            remove_interface(context, interface_name='wrong_number')
            send_or_edit(context, interface_name='wrong_number', text=_("Contact has no phone number"))
            return self.ask_phone(update, context)

        context.user_data[self.menu_name]['phone'] = phone
        remove_interface(context)
        context.bot.send_message(chat_id=user.chat_id, text=_("Phone number was accepted"), reply_markup=ReplyKeyboardRemove())
        return self.ask_name(update, context)

    def ask_name(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your full name and send it to me") + "</b>\n\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("📝 Application for membership (Step {step} of {of})").format(step=2, of=5)

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_phone')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.NAME

    def set_name(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            name = validators.String().to_python(update.effective_message.text)
            context.user_data[self.menu_name]["user_name"] = name
            user.desired_name = name
            add_to_db(user)
            delete_user_message(update)
            return self.ask_edrpou(update, context)
        except Invalid:
            remove_interface(context, 'wrong_name')
            send_or_edit(context, "wrong_name", chat_id=user.chat_id, text=_("Wrong name, please try again"))
            delete_interface(context)
            return self.ask_name(update, context)

    def ask_edrpou(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Please enter EDRPOU of your company and send it to me") + "\n\n"

        message_text += _("After filling all data assignment would be generated for confirmation of your powers") + "\n\n"

        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("📝 Application for membership (Step {step} of {of})").format(step=3, of=5)

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_name')]]

        if update.callback_query:
            update.callback_query.answer()
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ERDPOU

    def set_edrpou(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            edrpou = validators.String().to_python(update.effective_message.text)
            resp = requests.get("https://api.youscore.com.ua/v1/companyInfo/{edrpou}?apiKey={api_key}".format(edrpou=edrpou, api_key=YOUCONTROL_TOKEN))

            if resp.status_code == 404:
                message_text = _("⁉️ Company not found") + "\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += _("You can try again or fill assignment later") + '\n\n'
                message_text += "〰〰〰〰〰〰〰〰〰〰\n"
                message_text += _("📝 Application for membership (Step {step} of {of})").format(step=3, of=5)

                buttons = [[InlineKeyboardButton(_("✏️ Fill manually"), callback_data=f'fill_manually')],
                           [InlineKeyboardButton(_("⏪ Change"), callback_data=f'back_to_edrpou')]]

                delete_interface(context)
                send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
                return self.States.ERDPOU
            if resp.status_code == 400:
                send_or_edit(context, "wrong_edrpou", chat_id=user.chat_id, text=_("Wrong edrpou code, please try again"))
                delete_interface(context)
                return self.ask_edrpou(update, context)
            else:
                data = resp.json()
                context.user_data[self.menu_name]["edrpou"] = edrpou
                context.user_data[self.menu_name]["short_name"] = data['shortName']
                context.user_data[self.menu_name]["name"] = data['name']
                context.user_data[self.menu_name]["city"] = data["address"]
                context.user_data[self.menu_name]["owner"] = data["director"]
                delete_user_message(update)
                return self.approve_company(update, context)
        except Invalid:
            send_or_edit(context, "wrong_erdpou", chat_id=user.chat_id, text=_("Wrong edrpou code, please try again"))
            delete_interface(context)
            return self.ask_edrpou(update, context)

    def approve_company(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✅ Confirm company") + "\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += f"🏢 {context.user_data[self.menu_name]['short_name'] if context.user_data[self.menu_name]['short_name'] else context.user_data[self.menu_name]['name']}\n"
        message_text += f"🆔 {context.user_data[self.menu_name]['edrpou']}\n"
        message_text += f"🏠 {context.user_data[self.menu_name]['city']}\n"
        message_text += f"👨‍💼 {context.user_data[self.menu_name]['owner']}\n\n"

        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("📝 Application for membership (Step {step} of {of})").format(step=4, of=5)

        buttons = [[InlineKeyboardButton(_("✅ Confirm"), callback_data=f'confirm_company')],
                   [InlineKeyboardButton(_("⏪ Change"), callback_data=f'back_to_edrpou')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ASSIGNMENT

    def download_blank(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "👆👆👆\n"
        message_text += "<b>" + _("Your assignment") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("1️⃣ Download and print") + "\n"
        message_text += _("2️⃣ Fill empty field manually and sign") + "\n"
        message_text += _("3️⃣ Make a photo and <b>📎 Send assignment</b> to me now or later")

        buttons = [[InlineKeyboardButton(_("📎 Send now"), callback_data=f'send_assignment')],
                   [InlineKeyboardButton(_("⏳ Make it later"), callback_data=f'send_later')],
                   [InlineKeyboardButton(_("📨 Get blank on email"), callback_data=f'get_blank_on_email')],
                   [InlineKeyboardButton(_("🚫 Cancel"), callback_data=f"back_to_menu_{self.menu_name}")]]

        result_pdf = self.create_pdf_blank(context)
        with BytesIO() as output:
            result_pdf.write(output)
            output.seek(0)

            filename = _("Assignment blank") + '.pdf'
            send_or_edit(context, chat_id=user.chat_id, document=output, filename=filename, caption=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        # save request
        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.club == self.selected_object(context)) \
            .first()

        if request is None:
            request = JoinClubRequest()
            request.edrpou = context.user_data[self.menu_name]["edrpou"] if context.user_data[self.menu_name]["edrpou"] else "Empty"
            request.company = context.user_data[self.menu_name]["name"] if context.user_data[self.menu_name]["name"] else "Empty"
            request.owner = context.user_data[self.menu_name]["owner"] if context.user_data[self.menu_name]["owner"] else "Empty"
            request.city = context.user_data[self.menu_name]["city"] if context.user_data[self.menu_name]["city"] else "Empty"
            request.name = context.user_data[self.menu_name]["user_name"] if context.user_data[self.menu_name]["user_name"] else "Empty"
            request.phone = context.user_data[self.menu_name]["phone"] if context.user_data[self.menu_name]["phone"] else "Empty"
            request.email = context.user_data[self.menu_name]["email"]

            request.club = self.selected_object(context)
            request.user = user
            add_to_db(request, DBSession)

            # send notification
            requests_permission = DBSession.query(Permission).get(JoinClubRequest.view_permission)
            superuser_permission = DBSession.query(Permission).get("superuser")
            admins = DBSession.query(User) \
                .join(User.permissions) \
                .filter(User.permissions.contains(requests_permission) | User.permissions.contains(superuser_permission)) \
                .all()
            for admin in admins:
                _ = admin.translator
                buttons = [[InlineKeyboardButton(_("Close"), callback_data='close_message')]]
                remove_interface(context, 'admin_new_request_notification', user_id=admin.chat_id, dispatcher=self.dispatcher)
                send_or_edit(context, dispatcher=self.dispatcher, user_id=admin.chat_id, chat_id=admin.chat_id, interface_name='admin_new_request_notification', text=_("New club request was added."), reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.DOWNLOAD_BLANK

    def ask_email(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your email and send it to me") + "</b>\n\n"

        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("📝 Application for membership (Step {step} of {of})").format(step=5, of=5)

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_download")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.EMAIL

    def set_email(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            email = validators.Email().to_python(update.effective_message.text)
            context.user_data[self.menu_name]["email"] = email
            self.send_blank_to_email(context)
            delete_user_message(update)
            return self.email_sent_info(update, context)
        except Invalid:
            remove_interface(context, 'wrong_email')
            send_or_edit(context, "wrong_email", chat_id=user.chat_id, text=_("Wrong email, please try again"))
            delete_interface(context)
            return self.ask_email(update, context)

    def email_sent_info(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📨️ Assignment blank was sent") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        message_text += _("Got it? Ready sign now and <b>📎 Send assignment</b> to me?") + "\n\n"

        message_text += _("Can't find letter from us in your {email} box? Check SPAM folder or <b>📨 Change email</b>").format(email=context.user_data[self.menu_name]["email"])

        buttons = [[InlineKeyboardButton(_("📎 Send assignment"), callback_data=f'send_assignment')],
                   [InlineKeyboardButton(_("⏳ Send later"), callback_data=f'send_later')],
                   [InlineKeyboardButton(_("📨 Change email"), callback_data=f'back_to_email')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.AFTER_EMAIL

    def ask_assignment_after_download(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("📎 Select document and send it to me") + "\n\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_download')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.SEND

    def ask_assignment_after_email(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("📎 Select document and send it to me") + "\n\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_email')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.SEND

    def send_later(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("You can send signed assignment later in section:") + "\n"

        message_text += _("👨‍🌾 My clubs >>⏳ Waiting for assignment")

        buttons = [[InlineKeyboardButton(_("🏠 Main menu"), callback_data=f'main_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.SEND

    def send_blank_to_email(self, context):
        user = context.user_data['user']
        _ = user.translator
        email = context.user_data[self.menu_name]['email']

        message = MIMEMultipart()
        message["Subject"] = _("Your assignment for registration into best agro practices club")
        message["From"] = SENDER_EMAIL
        message["To"] = email

        content = "<p>" + _("An assignment was generated to confirm your authority.\n"
                            "To complete the registration, you must download, fill in the blank fields and sign it.") + "</p>\n"
        content += "<p>" + _("To complete your registration with the club, upload a photo or a scanned copy of the completed Order into the bot:\n"
                             "(1) Main Menu> (2) My Clubs> (3) Waiting for an Order> (4) Send an Order") + "</p>\n"

        html = """\
                 <html>
                   <body>
                     {content}
                   </body>
                 </html>
                 """.format(content=content.replace('\n', '<br/>'))

        html_part = MIMEText(html, "html")
        message.attach(html_part)

        result_pdf = self.create_pdf_blank(context)
        with BytesIO() as output:
            result_pdf.write(output)
            output.seek(0)
            attachedfile = MIMEApplication(output.read(), _subtype="pdf", _encoder=encode_base64)
            attachedfile.add_header('Content-Disposition', 'attachment', filename="Assignment blank.pdf")
            message.attach(attachedfile)

        # send mail
        ssl_context = ssl.create_default_context()
        with smtplib.SMTP_SSL(SENDER_SERVER, SENDER_SERVER_PORT, context=ssl_context) as server:
            server.login(SENDER_USERNAME, SENDER_PASSWORD)
            server.sendmail(SENDER_EMAIL, email, message.as_string())

    def accept_assignment(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        delete_user_message(update)
        message_text = "<b>" + _("✅ Registration is finished") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        message_text += _("We will notify you when check your request") + "\n\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("📝 Application for membership (Step {step} of {of})").format(step=5, of=5)

        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.club == self.selected_object(context)) \
            .first()
        if request is None:
            request = JoinClubRequest()

        request.edrpou = context.user_data[self.menu_name]["edrpou"] if context.user_data[self.menu_name]["edrpou"] else "Empty"
        request.company = context.user_data[self.menu_name]["name"] if context.user_data[self.menu_name]["name"] else "Empty"
        request.owner = context.user_data[self.menu_name]["owner"] if context.user_data[self.menu_name]["owner"] else "Empty"
        request.city = context.user_data[self.menu_name]["city"] if context.user_data[self.menu_name]["city"] else "Empty"
        request.name = context.user_data[self.menu_name]["user_name"] if context.user_data[self.menu_name]["user_name"] else "Empty"
        request.phone = context.user_data[self.menu_name]["phone"] if context.user_data[self.menu_name]["phone"] else "Empty"
        request.email = context.user_data[self.menu_name]["email"]

        request.club = self.selected_object(context)
        request.user = user
        request.assignment = update.message.photo[-1].file_id
        add_to_db(request, DBSession)

        buttons = [[InlineKeyboardButton(_("🏠 Main menu"), callback_data=f'main_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.SEND

    def create_pdf_blank(self, context):
        user = context.user_data['user']

        edrpou = context.user_data[self.menu_name]["edrpou"]
        company = context.user_data[self.menu_name]["name"]
        owner = context.user_data[self.menu_name]["owner"]
        full_name = context.user_data[self.menu_name]["user_name"]
        phone = context.user_data[self.menu_name]["phone"]
        email = context.user_data[self.menu_name]["email"]

        overlay = BytesIO()
        can = canvas.Canvas(overlay, pagesize=A4, bottomup=0)

        # date
        can.setFont("Arial-Bold", 9)
        date_name = re.sub(':.*?:', "", datetime.date.today().strftime('%d.%m.%Y'))
        can.drawString(432, 118, date_name)

        # company
        if company:
            can.setFont("Arial-Bold", 9)
            base_company_name_y = 155
            company_name_list = wrap(re.sub(':.*?:', "", company), 82)
            for idx, company_name in enumerate(company_name_list):
                can.drawString(60, base_company_name_y + idx * 24, company_name)

        # EDRPOU
        if edrpou:
            can.setFont("Arial-Bold", 9)
            edrpou_name = re.sub(':.*?:', "", str(edrpou))
            can.drawString(432, 179, edrpou_name)

        # owner
        if owner:
            can.setFont("Arial-Bold", 9)
            owner_name = re.sub(':.*?:', "", owner)
            can.drawString(60, 226, owner_name)

        # full_name
        if full_name:
            can.setFont("Arial-Bold", 9)
            full_name_name = re.sub(':.*?:', "", full_name)
            can.drawString(60, 322, full_name_name)

        # phone
        if phone:
            can.setFont("Arial-Bold", 9)
            phone_name = re.sub(':.*?:', "", phone)
            can.drawString(60, 409, phone_name)

        # email
        if email:
            can.setFont("Arial-Bold", 9)
            email_name = re.sub(':.*?:', "", email)
            can.drawString(307, 409, email_name)

        can.save()
        overlay.seek(0)

        overlay_pdf = PdfFileReader(overlay, strict=False)
        # read template PDF
        template_pdf = PdfFileReader(open(path.join(RESOURCES_FOLDER, f'assignment_blank.pdf'), 'rb'), strict=False)

        result_pdf = PdfFileWriter()
        template_page = template_pdf.getPage(0)
        template_page.mergePage(overlay_pdf.getPage(0))
        result_pdf.addPage(template_page)

        return result_pdf

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def goto_main_menu(self, update, context):
        user = context.user_data['user']
        self.parent.send_message(context)
        self.fake_callback_update(user, "back_user_clubs_menu")
        return ConversationHandler.END

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.ask_phone, pattern="^join_club$")],

                self.States.ASSIGNMENT: [CallbackQueryHandler(self.ask_edrpou, pattern="^back_to_edrpou$"),
                                         CallbackQueryHandler(self.download_blank, pattern="^confirm_company$")],

                self.States.PHONE: generate_regex_handlers("⏪ Back", self.back_to_menu) +
                                   [MessageHandler(Filters.text, self.wrong_phone),
                                    MessageHandler(Filters.contact, self.set_phone)],

                self.States.NAME: [CallbackQueryHandler(self.ask_phone, pattern="^back_to_phone$"),
                                   MessageHandler(Filters.text, self.set_name)],

                self.States.ERDPOU: [MessageHandler(Filters.text, self.set_edrpou),
                                     CallbackQueryHandler(self.download_blank, pattern="^fill_manually$"),
                                     CallbackQueryHandler(self.ask_edrpou, pattern="^back_to_edrpou$"),
                                     CallbackQueryHandler(self.ask_name, pattern="^back_to_name$")],

                self.States.DOWNLOAD_BLANK: [CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu_{self.menu_name}$"),
                                             CallbackQueryHandler(self.ask_assignment_after_download, pattern="^send_assignment$"),
                                             CallbackQueryHandler(self.ask_email, pattern="^get_blank_on_email$"),
                                             CallbackQueryHandler(self.send_later, pattern="^send_later$")],

                self.States.EMAIL: [CallbackQueryHandler(self.download_blank, pattern="^back_to_download$"),
                                    MessageHandler(Filters.text, self.set_email)],

                self.States.AFTER_EMAIL: [CallbackQueryHandler(self.ask_email, pattern="^back_to_email$"),
                                          CallbackQueryHandler(self.ask_assignment_after_email, pattern="^send_assignment$"),
                                          CallbackQueryHandler(self.send_later, pattern="^send_later$")],
                self.States.SEND: [CallbackQueryHandler(self.goto_main_menu, pattern="^main_menu$"),
                                   CallbackQueryHandler(self.email_sent_info, pattern="^back_to_email$"),
                                   CallbackQueryHandler(self.download_blank, pattern="^back_to_download$"),
                                   MessageHandler(Filters.photo, self.accept_assignment)]}
