import datetime
import enum
import re
import smtplib
import ssl
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO
from os import path
from textwrap import wrap

from PyPDF2 import PdfFileReader, PdfFileWriter
from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db, require_permission
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message, remove_interface
from formencode import Invalid, validators
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters, ConversationHandler

from src.models import DBSession, JoinClubRequest
from src.settings import RESOURCES_FOLDER, SENDER_EMAIL, SENDER_SERVER, SENDER_SERVER_PORT, SENDER_PASSWORD, SENDER_USERNAME


class WaitingAssignmentMenu(OneListMenu):
    menu_name = 'waiting_assignment_menu'
    model = JoinClubRequest

    class States(enum.Enum):
        ACTION = 1
        EMAIL = 2
        SEND = 3

    @require_permission('waiting_assignment_menu_access')
    def entry(self, update, context):
        return super(WaitingAssignmentMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']

        return DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.assignment == None) \
            .order_by(JoinClubRequest.create_date.desc()) \
            .all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^waiting_assignment$')]

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Join request deleted")

    def delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Are you sure you want to delete this join request?")

    def message_text(self, context, assignment):
        user = context.user_data['user']
        _ = user.translator

        if assignment:
            message_text = "<b>" + _("Best agricultural practices {region}").format(region=assignment.club.region.get_translation(user.language_code).name_declension) + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

            message_text += _("⚠️ To complete registration with the club, you must <b>📎 Submit assignment</b> with signature and seal") + "\n\n"
            message_text += _("The order form is filled in automatically from the information you provide. You can get it again <b>📨 Receive by Email</b> or <b>📃 Download</b> Form in Telegram") + "\n\n"


        else:
            message_text = _("There is no assignments yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("⚠️ Waiting for assignment") + ' (' + _("Club") + " " + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def object_buttons(self, context, club):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if club:
            buttons.append([InlineKeyboardButton(_("📎 Send assignment"), callback_data='send_assignment')])
            buttons.append([InlineKeyboardButton(_("📃 Download blank"), callback_data='download_blank')])
            buttons.append([InlineKeyboardButton(_("📨 Get blank by email"), callback_data='get_blank_by_email')])
            buttons.append([InlineKeyboardButton(_("🗑 Delete request"), callback_data=f'delete_{self.menu_name}')])

        return buttons

    def download_blank(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📃️ Assignment blank") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        result_pdf = self.create_pdf_blank(context)
        with BytesIO() as output:
            result_pdf.write(output)
            output.seek(0)

            filename = _("Assignment blank") + '.pdf'
            send_or_edit(context, chat_id=user.chat_id, document=output, filename=filename, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def ask_email(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your email and send it to me") + "</b>\n\n"

        message_text += _("To confirm your powers assignment was generated.\n"
                          "You must download it and sign to complete the registration") + "\n\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.EMAIL

    def set_email(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            email = validators.Email().to_python(update.effective_message.text)
            context.user_data[self.menu_name]["email"] = email
            self.send_blank_to_email(context)
            delete_user_message(update)
            return self.email_sent_info(update, context)
        except Invalid:
            send_or_edit(context, "wrong_email", chat_id=user.chat_id, text=_("Wrong email, please try again"))
            remove_interface(context, "wrong_email")
            delete_interface(context)
            return self.ask_email(update, context)

    def email_sent_info(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📨️ Assignment blank was sent") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        message_text += _("Got it? Ready sign now and <b>📎 Send assignment</b> to me?") + "\n\n"

        message_text += _("Can't find letter from us in your {email} box? Check SPAM folder or <b>📨 Change email</b>").format(email=context.user_data[self.menu_name]["email"])

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def send_blank_to_email(self, context):
        user = context.user_data['user']
        _ = user.translator
        email = context.user_data[self.menu_name]['email']

        message = MIMEMultipart()
        message["Subject"] = _("Your assignment for registration into best agro practices club")
        message["From"] = SENDER_EMAIL
        message["To"] = email

        content = "<p>" + _("An assignment was generated to confirm your authority.\n"
                            "To complete the registration, you must download, fill in the blank fields and sign it.") + "</p>\n"
        content += "<p>" + _("To complete your registration with the club, upload a photo or a scanned copy of the completed Order into the bot:\n"
                             "(1) Main Menu> (2) My Clubs> (3) Waiting for an Order> (4) Send an Order") + "</p>\n"

        html = """\
                 <html>
                   <body>
                     {content}
                   </body>
                 </html>
                 """.format(content=content.replace('\n', '<br/>'))

        html_part = MIMEText(html, "html")
        message.attach(html_part)
        pdf_attachment = MIMEApplication(open(path.join(RESOURCES_FOLDER, 'assignment_blank.pdf'), "rb").read(), _subtype="pdf")
        pdf_attachment.add_header('content-disposition', 'attachment', filename=('utf-8', '', 'assignment blank.pdf'))
        message.attach(pdf_attachment)

        # send mail
        ssl_context = ssl.create_default_context()
        with smtplib.SMTP_SSL(SENDER_SERVER, SENDER_SERVER_PORT, context=ssl_context) as server:
            server.login(SENDER_USERNAME, SENDER_PASSWORD)
            server.sendmail(SENDER_EMAIL, email, message.as_string())

    def ask_assignment(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("📎 Select document and send it to me") + "\n\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.SEND

    def accept_assignment(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        delete_user_message(update)
        message_text = "<b>" + _("✅ Registration is finished") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        message_text += _("We will notify you when check your request") + "\n\n"

        request = self.selected_object(context)
        request.assignment = update.message.photo[-1].file_id
        add_to_db(request, DBSession)

        buttons = [[InlineKeyboardButton(_("🏠 Main menu"), callback_data=f'main_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.SEND

    def create_pdf_blank(self, context):
        user = context.user_data['user']
        club_request = self.selected_object(context)

        edrpou = club_request.edrpou
        company = club_request.company
        owner = club_request.owner
        city = club_request.city
        club = club_request.club
        full_name = club_request.name
        phone = club_request.phone
        email = club_request.email

        overlay = BytesIO()
        can = canvas.Canvas(overlay, pagesize=A4, bottomup=0)

        # date
        can.setFont("Arial-Bold", 9)
        date_name = re.sub(':.*?:', "", datetime.date.today().strftime('%d.%m.%Y'))
        can.drawString(432, 118, date_name)

        # company
        can.setFont("Arial-Bold", 9)
        base_company_name_y = 155
        company_name_list = wrap(re.sub(':.*?:', "", company), 82)
        for idx, company_name in enumerate(company_name_list):
            can.drawString(60, base_company_name_y + idx * 24, company_name)

        # EDRPOU
        can.setFont("Arial-Bold", 9)
        edrpou_name = re.sub(':.*?:', "", str(edrpou))
        can.drawString(432, 179, edrpou_name)

        # owner
        can.setFont("Arial-Bold", 9)
        owner_name = re.sub(':.*?:', "", owner)
        can.drawString(60, 226, owner_name)

        # full_name
        can.setFont("Arial-Bold", 9)
        full_name_name = re.sub(':.*?:', "", full_name)
        can.drawString(60, 322, full_name_name)

        # phone
        can.setFont("Arial-Bold", 9)
        phone_name = re.sub(':.*?:', "", phone)
        can.drawString(60, 409, phone_name)

        # email
        if email:
            can.setFont("Arial-Bold", 9)
            email_name = re.sub(':.*?:', "", email)
            can.drawString(307, 409, email_name)

        can.save()
        overlay.seek(0)

        overlay_pdf = PdfFileReader(overlay, strict=False)
        # read template PDF
        template_pdf = PdfFileReader(open(path.join(RESOURCES_FOLDER, f'assignment_blank.pdf'), 'rb'), strict=False)

        result_pdf = PdfFileWriter()
        template_page = template_pdf.getPage(0)
        template_page.mergePage(overlay_pdf.getPage(0))
        result_pdf.addPage(template_page)

        return result_pdf

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def goto_main_menu(self, update, context):
        user = context.user_data['user']
        self.parent.send_message(context)
        self.fake_callback_update(user, "back_user_clubs_menu")
        return ConversationHandler.END

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.download_blank, pattern="^download_blank$"),
                                     CallbackQueryHandler(self.ask_assignment, pattern="^send_assignment$"),
                                     CallbackQueryHandler(self.ask_email, pattern="^get_blank_by_email$")],

                self.States.EMAIL: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                    MessageHandler(Filters.text, self.set_email)],

                self.States.SEND: [CallbackQueryHandler(self.goto_main_menu, pattern="^main_menu$"),
                                   CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                   MessageHandler(Filters.photo, self.accept_assignment)]}
