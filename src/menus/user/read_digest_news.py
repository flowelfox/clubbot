import datetime
import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db, prepare_user
from botmanlib.messages import send_or_edit, delete_user_message, remove_interface, delete_interface
from formencode import validators, Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, JoinClubRequest, News, Club, Report, User
from src.settings import WEBHOOK_ENABLE


class ReadNewsMenu(OneListMenu):
    menu_name = 'read_news_menu'
    disable_web_page_preview = False

    class States(enum.Enum):
        ACTION = 1
        REPORT = 2

    def entry(self, update, context):
        user = prepare_user(User, update, context, lang='uk')
        self._load(context)

        date_str = update.callback_query.data.replace("read_news_", "")
        context.user_data[self.menu_name]['date'] = datetime.datetime.strptime(date_str, "%d.%m.%Y")
        update.effective_message.delete()
        delete_interface(context)
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']

        regions = [user.region_id] + [club.region_id for club in user.clubs]
        news = DBSession.query(News) \
            .join(News.clubs) \
            .filter(News.create_date > datetime.datetime.utcnow() - datetime.timedelta(days=7)) \
            .filter(Club.region_id.in_(regions)) \
            .order_by(News.create_date.desc()) \
            .all()
        return news

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^read_news_\d+\.\d+.\d+$')]

    def message_text(self, context, news):
        user = context.user_data['user']
        _ = user.translator

        if news:
            message_text = ""
            if news.image:
                message_text = f'<a href="{self.bot.get_image_url(news.image)}">\u200B</a>' if news.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + news.title + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += news.create_date.strftime("%d.%m.%Y") + "\n\n"

            if len(news.text) > 100:
                message_text += news.text[:100] + "..."
            else:
                message_text += news.text
            message_text += "\n"

        else:
            message_text = _("There is no news yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator
        clubs = user.clubs

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        if len(clubs) > 1:
            message_text += "<i>" + _("🗞️ News of all your regions") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"
        else:
            message_text += "<i>" + _("🗞️ News of {region}").format(region=user.region.get_translation(user.language_code).name_declension) + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")" + "</i>"

        return message_text

    def object_buttons(self, context, news):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if news:
            buttons.append([InlineKeyboardButton(_("👍 {likes}").format(likes=len(news.likes)), callback_data="like_news"),
                            InlineKeyboardButton(_("⭐ Interested in {interested_in}").format(interested_in=len(news.interested_in)), callback_data="interested_in_news"),
                            InlineKeyboardButton(_("👎 {dislikes}").format(dislikes=len(news.dislikes)), callback_data="dislike_news")])
            buttons.append([InlineKeyboardButton(_("👀 More"), callback_data='more')])

        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("🏠 Main menu"), callback_data="start")

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        news = self.selected_object(context)
        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == news.user) \
            .filter(JoinClubRequest.club_id.in_([c.id for c in news.clubs])) \
            .first()

        message_text = "<b>" + news.title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += news.text + "\n\n"
        author_link = f'<a href="{news.user.mention_url}">{news.user.get_name()}</a>' if user.clubs else news.user.get_name()
        message_text += "<b>" + _("© Author:") + "</b> " + f'{author_link} ({request.company})\n\n'

        buttons = [[InlineKeyboardButton(_("🚨 Report"), callback_data='report_post')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def ask_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("✍️ Enter your report text and send it to me") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.REPORT

    def set_report(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            text = validators.String().to_python(update.effective_message.text)

            report = Report()
            report.text = text
            report.user = user
            report.news = self.selected_object(context)
            add_to_db(report)

            delete_user_message(update)

            message_text = "<b>" + _("✅ Your report was sent") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')]]

            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

            return self.States.REPORT
        except Invalid:
            remove_interface(context, 'wrong_text')
            send_or_edit(context, "wrong_text", chat_id=user.chat_id, text=_("Wrong text, please try again"))
            delete_interface(context)
            return self.ask_report(update, context)

    def like(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        news = self.selected_object(context)
        if user in news.likes:
            news.likes.remove(user)
        else:
            news.likes.append(user)

        if user in news.dislikes:
            news.dislikes.remove(user)

        if user in news.interested_in:
            news.interested_in.remove(user)

        add_to_db(news, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def dislike(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        news = self.selected_object(context)
        if user in news.dislikes:
            news.dislikes.remove(user)
        else:
            news.dislikes.append(user)

        if user in news.likes:
            news.likes.remove(user)

        if user in news.interested_in:
            news.interested_in.remove(user)

        add_to_db(news, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def interested_in(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        news = self.selected_object(context)
        if user in news.interested_in:
            news.interested_in.remove(user)
        else:
            news.interested_in.append(user)

        if user in news.likes:
            news.likes.remove(user)

        if user in news.dislikes:
            news.dislikes.remove(user)

        add_to_db(news, DBSession)

        self._get_objects(context)
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.more, pattern="^more$"),
                                     CallbackQueryHandler(self.like, pattern="^like_news$"),
                                     CallbackQueryHandler(self.dislike, pattern="^dislike_news$"),
                                     CallbackQueryHandler(self.interested_in, pattern="^interested_in_news$"),
                                     CallbackQueryHandler(self.ask_report, pattern="^report_post$")],
                self.States.REPORT: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     MessageHandler(Filters.text, self.set_report)]}
