import datetime
import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, get_settings, require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.models import DBSession, JoinClubRequest, News
from src.settings import WEBHOOK_ENABLE, SETTINGS_FILE


class CreateNewsMenu(BaseMenu):
    menu_name = 'create_news_menu'

    class States(enum.Enum):
        ACTION = 1
        TITLE = 2
        TEXT = 3
        IMAGE = 4
        CONFIRMATION = 5

    @require_permission('allow_create_news')
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        user = context.user_data['user']
        _ = user.translator

        return self.ask_title(update, context)

    def ask_title(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter a title for the news and send it to me") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🗞️ Create news (Step {step} of {of})").format(step=1, of=4) + "</i>"

        buttons = [[InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.TITLE

    def set_title(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        context.user_data[self.menu_name]['title'] = text

        delete_user_message(update)
        return self.ask_text(update, context)

    def ask_text(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter a text for the news and send it to me") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🗞️ Create news (Step {step} of {of})").format(step=2, of=4) + "</i>"

        buttons = [[InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_title')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.TEXT

    def set_text(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        context.user_data[self.menu_name]['text'] = text

        delete_user_message(update)
        return self.ask_image(update, context)

    def ask_image(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("📎 Pick a photo for the news from the gallery or take it and send it to me") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🗞️ Create news (Step {step} of {of})").format(step=3, of=4) + "</i>"

        buttons = [[InlineKeyboardButton(_("Next (without photo) ⏩"), callback_data='skip_image')],
                   [InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_text')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.IMAGE

    def set_image(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        context.user_data[self.menu_name]['image'] = update.effective_message.photo[-1].file_id

        delete_user_message(update)
        return self.created(update, context)

    def created(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        settings = get_settings(SETTINGS_FILE)
        digest_time = datetime.time(settings['news_digest']['hour'], settings['news_digest']['minute'])
        today = datetime.date.today()

        digest_date = datetime.datetime.combine(today, digest_time)
        while today.weekday() != settings['news_digest']['day'] and digest_date > datetime.datetime.utcnow():
            today += datetime.timedelta(days=1)
            digest_date = datetime.datetime.combine(today, digest_time)

        message_text = "<b>" + _("✅ Your news created") + '</b>\n\n'
        message_text += _("To publish it in the next digest ({digest_date}) check with <b>👁️ Preview</b> and confirm").format(digest_date=digest_date.strftime("%H:%M %d.%m.%Y")) + "\n\n"

        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🗞️ Create news (Step {step} of {of})").format(step=4, of=4) + "</i>"

        buttons = [[InlineKeyboardButton(_("👁️ Preview"), callback_data='preview')],
                   [InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_image')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.CONFIRMATION

    def preview(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        title = context.user_data[self.menu_name]['title']
        text = context.user_data[self.menu_name]['text']
        image = context.user_data[self.menu_name].get('image', None)
        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.club == context.user_data[self.parent.menu_name]['clubs'][0]) \
            .first()

        message_text = ""
        if image:
            message_text = f'<a href="{self.bot.get_image_url(image)}">\u200B</a>' if image and WEBHOOK_ENABLE else ""
        message_text += "<b>" + title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += datetime.date.today().strftime("%d.%m.%Y") + "\n\n"

        if len(text) > 100:
            message_text += text[:100] + "..."
        else:
            message_text += text
        message_text += "\n\n"
        message_text += "<b>" + _("© Author:") + "</b> " + f'<a href="{user.mention_url}">{user.get_name()}</a> ({request.company})\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🗞️ Create news (Preview)") + "</i>"

        buttons = [[InlineKeyboardButton(_("✅ Confirm"), callback_data='confirm'),
                    InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')],
                   [InlineKeyboardButton(_("👀 More"), callback_data='more')],
                   [InlineKeyboardButton(_("⏪ Edit"), callback_data='back_to_created')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.CONFIRMATION

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        title = context.user_data[self.menu_name]['title']
        text = context.user_data[self.menu_name]['text']

        message_text = "<b>" + title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += text + "\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='preview')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.CONFIRMATION

    def create_news(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        title = context.user_data[self.menu_name]['title']
        text = context.user_data[self.menu_name]['text']
        image = context.user_data[self.menu_name].get('image', None)

        news = News(title=title, text=text, image=image, user=user)

        for club in context.user_data[self.parent.menu_name]['clubs']:
            news.clubs.append(club)

        if not add_to_db(news, DBSession):
            return self.conv_fallback(context)

        settings = get_settings(SETTINGS_FILE)
        digest_time = datetime.time(settings['news_digest']['hour'], settings['news_digest']['minute'])
        today = datetime.date.today()

        digest_date = datetime.datetime.combine(today, digest_time)
        while today.weekday() != settings['news_digest']['day'] and digest_date > datetime.datetime.utcnow():
            today += datetime.timedelta(days=1)
            digest_date = datetime.datetime.combine(today, digest_time)

        update.callback_query.answer(text=_("🎉 Congratulations!\n\nYour news was created and will be sent in the next club digest ({digest_date}) to all registered operators").format(digest_date=digest_date.strftime("%H:%M %d.%m.%Y")), show_alert=True)

        self.fake_callback_update(user, "back_announcement_menu")
        return self.back(update, context)

    def back(self, update, context):
        self.parent.ask_type(update, context)
        if update.callback_query and update.callback_query.id != 0:
            update.callback_query.answer()
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^create_news$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$')],
                                          self.States.TITLE: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                              MessageHandler(Filters.text, self.set_title)],
                                          self.States.TEXT: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                             CallbackQueryHandler(self.ask_title, pattern='^back_to_title$'),
                                                             MessageHandler(Filters.text, self.set_text)],
                                          self.States.IMAGE: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                              CallbackQueryHandler(self.ask_text, pattern=f'^back_to_text$'),
                                                              CallbackQueryHandler(self.created, pattern=f'^skip_image$'),
                                                              MessageHandler(Filters.photo, self.set_image)],
                                          self.States.CONFIRMATION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                                     CallbackQueryHandler(self.ask_image, pattern=f'^back_to_image$'),
                                                                     CallbackQueryHandler(self.created, pattern=f'^back_to_created$'),
                                                                     CallbackQueryHandler(self.preview, pattern='^preview$'),
                                                                     CallbackQueryHandler(self.more, pattern='^more$'),
                                                                     CallbackQueryHandler(self.create_news, pattern='^confirm$'),
                                                                     ],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
