import enum

from botmanlib.menus import BunchListMenu
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, prepare_user, require_permission, inline_placeholder, group_buttons
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message
from telegram import TelegramError, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler, PrefixHandler, CallbackContext

from src.menus.user.clubs import ClubsMenu
from src.menus.user.digest import DigestMenu
from src.menus.user.profile import ProfileMenu
from src.models import User, DBSession, RegionTranslation


class StartMenu(BaseMenu):
    menu_name = 'start_menu'

    class States(enum.Enum):
        ACTION = 1
        LANGUAGE = 2

    def entry(self, update, context):
        user = prepare_user(User, update, context, 'uk')

        _ = user.translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if update.effective_message and update.effective_message.text and update.effective_message.text.startswith("/"):
            delete_interface(context)

        if update.callback_query and update.callback_query.data == 'start':
            update.callback_query.answer()
            try:
                update.effective_message.edit_reply_markup()
            except (TelegramError, AttributeError):
                pass

        if not user.has_permission('start_menu_access'):
            self.bot.send_message(chat_id=user.chat_id, text=_("You were restricted from using this bot"))
            return self.States.ACTION

        self.clear_all_states(context, user.chat_id)
        if user.region is None:
            self.fake_callback_update(user, "select_region")
            return self.States.ACTION
        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("🏠 Main menu") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("Select section:")
        clubs_count = len(self.clubs_menu.my_clubs_menu.query_objects(context)) + len(self.clubs_menu.waiting_assignment_menu.query_objects(context)) + len(self.clubs_menu.on_moderation_menu.query_objects(context))

        buttons = [[InlineKeyboardButton(_("📬 Regions digest"), callback_data='digest')],
                   [InlineKeyboardButton(_("👨‍🌾 My clubs") + f" ({clubs_count})", callback_data='my_clubs_menu')],
                   [InlineKeyboardButton(_("🚨 AGRO SOS"), callback_data='agro_sos')],
                   [InlineKeyboardButton(_("👤 My profile"), callback_data='profile')],
                   [InlineKeyboardButton(_("🙌 Share bot"), switch_inline_query=_(" – the first bot to bring together the best agricultural practices!\n📲 Establish to be aware of the agrarian life of your region and to take it wholeheartedly"))]]

        if user.has_permission("admin_menu_access"):
            buttons.append([InlineKeyboardButton(_("⚙️ Administrator menu"), callback_data='admin')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    @require_permission("change_language_menu_access")
    def ask_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton("🇬🇧ENG", callback_data='language_en')],
                   [InlineKeyboardButton("🇺🇦UKR", callback_data='language_uk')],
                   [InlineKeyboardButton("🇷🇺RUS", callback_data='language_ru')]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Select a language:"),
                     reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LANGUAGE

    def start_ask_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "Якою мовою бажаєш спілкуватися?\n" \
                       "In what language do you want to communicate?\n" \
                       "На каком языке ты хочешь общаться?"
        buttons = [[InlineKeyboardButton("🇺🇦Українська", callback_data='language_uk')],
                   [InlineKeyboardButton("🇷🇺Русский", callback_data='language_ru')],
                   [InlineKeyboardButton("🇬🇧English", callback_data='language_en')],
                   ]

        send_or_edit(context, chat_id=user.chat_id, text=message_text,
                     reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LANGUAGE

    def set_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        value = update.callback_query.data.replace("language_", "")

        if value == 'en':
            user.language_code = 'en'
        elif value == 'ru':
            user.language_code = 'ru'
        elif value == 'uk':
            user.language_code = 'uk'
        else:
            user.language_code = 'en'

        add_to_db(user)
        context.user_data['_'] = _ = user.translator

        self.send_message(context)

        return self.States.ACTION

    def goto_next_menu(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        profile_menu = ProfileMenu(self)
        self.clubs_menu = ClubsMenu(self)
        digest_menu = DigestMenu(self)
        select_region_menu = SelectRegionMenu(self)
        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'start', self.entry),
                                                    CallbackQueryHandler(self.entry, pattern='^start$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.ask_language, pattern="^language$"),
                                                               select_region_menu.handler,
                                                               profile_menu.handler,
                                                               self.clubs_menu.handler,
                                                               digest_menu.handler,
                                                               CallbackQueryHandler(self.goto_next_menu, pattern="^admin$"),
                                                               CallbackQueryHandler(inline_placeholder(self.States.ACTION, "Under construction"), pattern="agro_sos")],
                                          self.States.LANGUAGE: [CallbackQueryHandler(self.set_language, pattern="^language_\w\w$")],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.regex('^/admin$'), self.goto_next_menu),
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class SelectRegionMenu(BunchListMenu):
    menu_name = "select_region_menu"
    objects_per_page = 8

    def query_objects(self, context: CallbackContext):
        user = context.user_data['user']
        return DBSession.query(RegionTranslation).filter(RegionTranslation.lang == user.language_code).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^select_region$")]

    def message_text(self, context, objects):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📍 Select your region") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("Digest of that region you will be getting every week") + "</i>\n"

        return message_text

    def page_text(self, current_page, max_page, context: CallbackContext):
        _ = context.user_data['user'].translator

        return "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

    def object_buttons(self, context, regions):
        flat_buttons = []
        for region in regions:
            flat_buttons.append(InlineKeyboardButton(region.name, callback_data=f"region_{region.id}"))

        return group_buttons(flat_buttons)

    def set_region(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        region_trans_id = int(update.callback_query.data.replace("region_", ""))
        region = DBSession.query(RegionTranslation).get(region_trans_id).region

        user.region = region
        add_to_db(user, DBSession)

        if update.callback_query:
            update.callback_query.answer()

        self.parent.send_message(context)
        return ConversationHandler.END

    def back_button(self, context):
        return None

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.set_region, pattern="^region_\d+$")]}
