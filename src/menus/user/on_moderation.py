import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, TelegramError
from telegram.ext import CallbackQueryHandler

from src.models import DBSession, JoinClubRequest


class OnModerationMenu(OneListMenu):
    menu_name = 'on_moderation_menu'
    model = JoinClubRequest

    class States(enum.Enum):
        ACTION = 1

    @require_permission('on_moderation_menu_access')
    def entry(self, update, context):
        return super(OnModerationMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']

        return DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.type == None) \
            .filter(JoinClubRequest.assignment != None) \
            .order_by(JoinClubRequest.create_date.desc()) \
            .all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^on_moderation$')]

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Join request deleted")

    def delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Are you sure you want to delete this join request?")

    def message_text(self, context, request):
        user = context.user_data['user']
        _ = user.translator

        if request:
            message_text = "<b>" + _("Best agricultural practices {region}").format(region=request.club.region.get_translation(user.language_code).name_declension) + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += f"🏢 {request.company}\n"
            message_text += f"🆔 {request.edrpou}\n"
            message_text += f"🏠 {request.city}\n"
            message_text += f"👨‍💼 {request.owner}\n"
        else:
            message_text = _("There is no requests yet") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("⏳ On moderation") + ' (' + _("Club") + " " + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def object_buttons(self, context, club):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if club:
            buttons.append([InlineKeyboardButton(_("📃 Download assignment"), callback_data='show_assignment')])

        return buttons

    def show_assignment(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        request = self.selected_object(context)
        buttons = [[InlineKeyboardButton(_("🔙 Back"), callback_data="back_to_menu")]]

        try:
            send_or_edit(context, chat_id=user.chat_id, photo=request.assignment, reply_markup=InlineKeyboardMarkup(buttons))
        except TelegramError:
            send_or_edit(context, chat_id=user.chat_id, text=_("Can't display assignment"), reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.ACTION

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.show_assignment, pattern=r"^show_assignment$")]}
