import datetime
import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, get_settings, require_permission
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface, remove_interface
from botmanlib.validators import DateTimeConverter
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.models import DBSession, JoinClubRequest, Offer
from src.settings import WEBHOOK_ENABLE, SETTINGS_FILE


class CreateOfferMenu(BaseMenu):
    menu_name = 'create_offer_menu'

    class States(enum.Enum):
        ACTION = 1
        TITLE = 2
        TEXT = 3
        ADDRESS = 4
        START_DATE = 5
        END_DATE = 6
        IMAGE = 7
        CONFIRMATION = 8

    @require_permission('allow_create_offers')
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['title'] = None
        context.user_data[self.menu_name]['text'] = None
        context.user_data[self.menu_name]['address'] = None
        context.user_data[self.menu_name]['start_date'] = None
        context.user_data[self.menu_name]['end_date'] = None
        context.user_data[self.menu_name]['image'] = None

        user = context.user_data['user']
        _ = user.translator

        return self.ask_title(update, context)

    def ask_title(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter a title for the offer and send it to me") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=1, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.TITLE

    def set_title(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        context.user_data[self.menu_name]['title'] = text

        delete_user_message(update)
        return self.ask_text(update, context)

    def ask_text(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter a detailed description of the offer and send it to me (including contacts, website)") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=2, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_title')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.TEXT

    def set_text(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        context.user_data[self.menu_name]['text'] = text

        delete_user_message(update)
        return self.ask_address(update, context)

    def ask_address(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter the full address where the offer will take place and send it to me") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=3, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("Next (without address) ⏩"), callback_data='skip_address')],
                   [InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_text')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ADDRESS

    def set_address(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        context.user_data[self.menu_name]['address'] = text

        delete_user_message(update)
        return self.ask_start_date(update, context)

    def ask_start_date(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter start time and date of the offer and send it to me in the format: HH:MM DD.MM.YYYY") + '\n\n'
        message_text += "<i>" + _("Example: 10:00 01.01.2020") + "</i>\n\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=4, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_address')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.START_DATE

    def set_start_date(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        delete_user_message(update)

        try:
            val = DateTimeConverter()
            text = update.effective_message.text
            context.user_data[self.menu_name]['start_date'] = val.to_python(text)
            return self.ask_end_date(update, context)
        except Invalid:
            send_or_edit(context, "wrong_start_time", chat_id=user.chat_id, text=_("Wrong date format, please try again"))
            remove_interface(context, "wrong_start_time")
            delete_interface(context)
            return self.ask_start_date(update, context)

    def ask_end_date(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("✍️ Enter end time and date of the offer and send it to me in the format: HH:MM DD.MM.YYYY") + '\n\n'
        message_text += "<i>" + _("Example: 10:00 01.01.2020") + "</i>\n\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=5, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_start_date')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.END_DATE

    def set_end_date(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        delete_user_message(update)

        try:
            val = DateTimeConverter()
            text = update.effective_message.text
            context.user_data[self.menu_name]['end_date'] = val.to_python(text)
            return self.ask_image(update, context)
        except Invalid:
            send_or_edit(context, "wrong_end_time", chat_id=user.chat_id, text=_("Wrong date format, please try again"))
            remove_interface(context, "wrong_end_time")
            delete_interface(context)
            return self.ask_start_date(update, context)

    def ask_image(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("📎 Pick a photo for the offer from the gallery or take it and send it to me") + '\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=6, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("Next (without photo) ⏩"), callback_data='skip_image')],
                   [InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_end_date')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.IMAGE

    def set_image(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        context.user_data[self.menu_name]['image'] = update.effective_message.photo[-1].file_id

        delete_user_message(update)
        return self.created(update, context)

    def created(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        settings = get_settings(SETTINGS_FILE)
        digest_time = datetime.time(settings['offers_digest']['hour'], settings['offers_digest']['minute'])
        today = datetime.date.today()

        digest_date = datetime.datetime.combine(today, digest_time)
        while today.weekday() != settings['offers_digest']['day'] and digest_date > datetime.datetime.utcnow():
            today += datetime.timedelta(days=1)
            digest_date = datetime.datetime.combine(today, digest_time)

        message_text = "<b>" + _("✅ Your offer created") + '</b>\n\n'
        message_text += _("To publish it in the next digest ({digest_date}) check with <b>👁️ Preview</b> and confirm").format(digest_date=digest_date.strftime("%H:%M %d.%m.%Y")) + "\n\n"

        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Step {step} of {of})").format(step=7, of=7) + "</i>"

        buttons = [[InlineKeyboardButton(_("👁️ Preview"), callback_data='preview')],
                   [InlineKeyboardButton(_("⏪ One step back"), callback_data='back_to_image')],
                   [InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.CONFIRMATION

    def preview(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        title = context.user_data[self.menu_name]['title']
        text = context.user_data[self.menu_name]['text']
        address = context.user_data[self.menu_name]['address']
        start_date = context.user_data[self.menu_name]['start_date']
        end_date = context.user_data[self.menu_name]['end_date']
        image = context.user_data[self.menu_name].get('image', None)

        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.club == context.user_data[self.parent.menu_name]['clubs'][0]) \
            .first()

        message_text = ""
        if image:
            message_text = f'<a href="{self.bot.get_image_url(image)}">\u200B</a>' if image and WEBHOOK_ENABLE else ""
        message_text += "<b>" + title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("⏱️ Start:") + "</b> " + start_date.strftime("%H:%M %d.%m.%Y") + "\n"
        message_text += "<b>" + _("⏱️ End:") + "</b> " + end_date.strftime("%H:%M %d.%m.%Y") + "\n"
        message_text += "<b>" + _("📍️ Address:") + "</b> " + address + "\n\n"

        if len(text) > 100:
            message_text += text[:100] + "..."
        else:
            message_text += text
        message_text += "\n\n"
        message_text += "<b>" + _("© Author:") + "</b> " + f'<a href="{user.mention_url}">{user.get_name()}</a> ({request.company})\n\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<i>" + _("🔖 Create offer (Preview)") + "</i>"

        buttons = [[InlineKeyboardButton(_("✅ Confirm"), callback_data='confirm'),
                    InlineKeyboardButton(_("🚫 ️Cancel"), callback_data=f'back_{self.menu_name}')],
                   [InlineKeyboardButton(_("👀 More"), callback_data='more')],
                   [InlineKeyboardButton(_("⏪ Edit"), callback_data='back_to_created')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.CONFIRMATION

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        title = context.user_data[self.menu_name]['title']
        text = context.user_data[self.menu_name]['text']

        message_text = "<b>" + title + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += text + "\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='preview')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.CONFIRMATION

    def create_offer(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        title = context.user_data[self.menu_name]['title']
        text = context.user_data[self.menu_name]['text']
        address = context.user_data[self.menu_name]['address']
        start_date = context.user_data[self.menu_name]['start_date']
        end_date = context.user_data[self.menu_name]['end_date']
        image = context.user_data[self.menu_name].get('image', None)

        offer = Offer(title=title, text=text, address=address, start_date=start_date, end_date=end_date, image=image, user=user)

        for club in context.user_data[self.parent.menu_name]['clubs']:
            offer.clubs.append(club)

        if not add_to_db(offer, DBSession):
            return self.conv_fallback(context)

        settings = get_settings(SETTINGS_FILE)
        digest_time = datetime.time(settings['offers_digest']['hour'], settings['offers_digest']['minute'])
        today = datetime.date.today()

        digest_date = datetime.datetime.combine(today, digest_time)
        while today.weekday() != settings['offers_digest']['day'] and digest_date > datetime.datetime.utcnow():
            today += datetime.timedelta(days=1)
            digest_date = datetime.datetime.combine(today, digest_time)

        update.callback_query.answer(text=_("🎉 Congratulations!\n\nYour offer was created and will be sent in the next club digest ({digest_date}) to all registered operators").format(digest_date=digest_date.strftime("%H:%M %d.%m.%Y")), show_alert=True)
        self.fake_callback_update(user, "back_announcement_menu")
        return self.back(update, context)

    def back(self, update, context):
        self.parent.ask_type(update, context)
        if update.callback_query and update.callback_query.id != 0:
            update.callback_query.answer()
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^create_offer$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$')],
                                          self.States.TITLE: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                              MessageHandler(Filters.text, self.set_title)],
                                          self.States.TEXT: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                             CallbackQueryHandler(self.ask_title, pattern='^back_to_title$'),
                                                             MessageHandler(Filters.text, self.set_text)],
                                          self.States.ADDRESS: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                                CallbackQueryHandler(self.ask_text, pattern='^back_to_text$'),
                                                                CallbackQueryHandler(self.ask_start_date, pattern=f'^skip_address$'),
                                                                MessageHandler(Filters.text, self.set_address)],
                                          self.States.START_DATE: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                                   CallbackQueryHandler(self.ask_address, pattern='^back_to_address$'),
                                                                   MessageHandler(Filters.text, self.set_start_date)],
                                          self.States.END_DATE: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                                 CallbackQueryHandler(self.ask_start_date, pattern='^back_to_start_date$'),
                                                                 MessageHandler(Filters.text, self.set_end_date)],
                                          self.States.IMAGE: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                              CallbackQueryHandler(self.ask_end_date, pattern=f'^back_to_end_date$'),
                                                              CallbackQueryHandler(self.created, pattern=f'^skip_image$'),
                                                              MessageHandler(Filters.photo, self.set_image)],
                                          self.States.CONFIRMATION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                                     CallbackQueryHandler(self.ask_image, pattern=f'^back_to_image$'),
                                                                     CallbackQueryHandler(self.created, pattern=f'^back_to_created$'),
                                                                     CallbackQueryHandler(self.preview, pattern='^preview$'),
                                                                     CallbackQueryHandler(self.more, pattern='^more$'),
                                                                     CallbackQueryHandler(self.create_offer, pattern='^confirm$'),
                                                                     ],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
