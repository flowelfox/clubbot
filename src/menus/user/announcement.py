from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import group_buttons, require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.user.create_event import CreateEventMenu
from src.menus.user.create_news import CreateNewsMenu
from src.menus.user.create_offer import CreateOfferMenu
from src.models import DBSession, Club, JoinClubRequest, ParticipantType


class AnnouncementMenu(BaseMenu):
    menu_name = 'announcement_menu'

    @require_permission('announcement_menu_access')
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        user = context.user_data['user']
        _ = user.translator
        if not user.clubs:
            update.callback_query.answer(text=_("⚠️ Only club members can make announcements!\n\nYou can choose a club and apply for membership in the 👨‍🌾 My clubs main menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📣 Create announcement") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("Select the club you want to announce to the members:")

        # buttons = [[InlineKeyboardButton(_("🗺️ All my clubs"), callback_data='club_all')]]
        buttons = []

        flat_buttons = []
        for club in user.clubs:
            flat_buttons.append(InlineKeyboardButton(club.region.get_translation(user.language_code).name_declension, callback_data=f"club_{club.id}"))

        buttons.extend(group_buttons(flat_buttons, 1))
        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_club(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if update.callback_query.data == "club_all":
            context.user_data[self.menu_name]['clubs'] = user.clubs
        else:
            club_id = int(update.callback_query.data.replace("club_", ""))
            context.user_data[self.menu_name]['clubs'] = [DBSession.query(Club).get(club_id)]

        return self.ask_type(update, context)

    def ask_type(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📣 Create announcement") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("Choose the section for your announcement:")

        buttons = [[InlineKeyboardButton(_("🗞️️ News"), callback_data='type_news')],
                   [InlineKeyboardButton(_("🗓️ Events"), callback_data='type_events')]]

        request = DBSession.query(JoinClubRequest) \
            .filter(JoinClubRequest.user == user) \
            .filter(JoinClubRequest.club == context.user_data[self.menu_name]['clubs'][0]) \
            .first()
        if request.type in [ParticipantType.honorable_member, ParticipantType.business_partner, ParticipantType.organ_of_state_power]:
            buttons.append([InlineKeyboardButton(_("🔖️️ Special offers"), callback_data='type_offers')])

        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_to_menu')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_type(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        type = update.callback_query.data.replace("type_", "")
        context.user_data[self.menu_name]['type'] = type
        if type == 'news':
            self.fake_callback_update(user, "create_news")
            update.callback_query.answer()
        elif type == 'events':
            self.fake_callback_update(user, "create_event")
            update.callback_query.answer()
        elif type == 'offers':
            self.fake_callback_update(user, "create_offer")
            update.callback_query.answer()
        else:
            update.callback_query.answer(text=_("Wrong choice"), show_alert=True)

        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        if update.callback_query and update.callback_query.id != 0:
            update.callback_query.answer()
        return ConversationHandler.END

    def get_handler(self):
        create_news_menu = CreateNewsMenu(self)
        create_event_menu = CreateEventMenu(self)
        create_offer_menu = CreateOfferMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^create_announcement$')],
                                      states={
                                          self.States.ACTION: [create_news_menu.handler,
                                                               create_event_menu.handler,
                                                               create_offer_menu.handler,
                                                               CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               CallbackQueryHandler(self.back_to_menu, pattern=f'^back_to_menu$'),
                                                               CallbackQueryHandler(self.set_club, pattern=r"^club_\d+$"),
                                                               CallbackQueryHandler(self.set_club, pattern=r"^club_all$"),
                                                               CallbackQueryHandler(self.set_type, pattern=r"^type_\w+$")],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
