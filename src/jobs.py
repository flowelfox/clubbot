import datetime
import logging

from botmanlib.menus.helpers import get_settings
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import Unauthorized, BadRequest

from src.models import BlockSession, User, NewsDigestSession, News, Club, EventsDigestSession, Event, OffersDigestSession, Offer
from src.settings import SETTINGS_FILE, SERVER_TIME_CORRECTION


def start_check_blocked_job(job_queue):
    stop_check_blocked_job(job_queue)
    job_queue.run_repeating(check_blocked_job, 60 * 120, first=0, name="check_blocked_job")
    logging.info("Check blocked users job started...")


def stop_check_blocked_job(job_queue):
    for job in job_queue.get_jobs_by_name("check_blocked_job"):
        job.schedule_removal()
        logging.info("Check blocked users job stopped...")


def check_blocked_job(context):
    users = BlockSession.query(User).all()
    for user in users:
        try:
            context.bot.send_chat_action(user.chat_id, 'typing')
        except (Unauthorized, BadRequest):
            user.deactivate()
            BlockSession.add(user)

    BlockSession.commit()


def start_news_digest_job(job_queue):
    stop_news_digest_job(job_queue)
    settings = get_settings(SETTINGS_FILE)
    job_queue.run_daily(news_digest_job, datetime.time(settings['news_digest']['hour'] + SERVER_TIME_CORRECTION, settings['news_digest']['minute']), (settings['news_digest']['day'],), name="news_digest_job")
    # job_queue.run_repeating(news_digest_job, 10, first=0, name="news_digest_job")
    logging.info("News digest job started...")


def stop_news_digest_job(job_queue):
    for job in job_queue.get_jobs_by_name("news_digest_job"):
        job.schedule_removal()
        logging.info("News digest job stopped...")


def news_digest_job(context):
    users = NewsDigestSession.query(User).filter(User.is_active == True).filter(User.region != None).all()

    for user in users:
        today = datetime.date.today()
        _ = user.translator
        regions = list(set([user.region] + [club.region for club in user.clubs]))
        news = NewsDigestSession.query(News) \
            .join(News.clubs) \
            .filter(News.create_date > datetime.datetime.utcnow() - datetime.timedelta(days=7)) \
            .filter(Club.region_id.in_([r.id for r in regions])) \
            .all()

        if len(regions) > 1:
            message_text = ""
            for region in regions:
                region_news = [n for n in news if (region.id in [c.region_id for c in n.clubs])]
                if not region_news:
                    continue

                message_text += "<b>" + _("🗞 News of {region_name} for last week").format(region_name=region.get_translation(user.language_code).name_declension) + '</b>\n'
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                for new in region_news:
                    message_text += f"🔸 {new.create_date.strftime('%d.%m.%Y')} {new.title}\n"
                message_text += "\n"
        else:
            message_text = "<b>" + _("🗞 News of {region_name} for last week").format(region_name=user.region.get_translation(user.language_code).name_declension) + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            for new in news:
                message_text += f"🔸 {new.create_date.strftime('%d.%m.%Y')} {new.title}\n"

        buttons = [[InlineKeyboardButton(_("👀 Read news") + f" ({len(news)})", callback_data=f"read_news_{today.strftime('%d.%m.%Y')}")],
                   [InlineKeyboardButton(_("❎ Close message"), callback_data="close_message")]]
        if news:
            context.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")


def start_events_digest_job(job_queue):
    stop_events_digest_job(job_queue)
    settings = get_settings(SETTINGS_FILE)
    job_queue.run_daily(events_digest_job, datetime.time(settings['events_digest']['hour'] + SERVER_TIME_CORRECTION, settings['events_digest']['minute']), (settings['events_digest']['day'],), name="events_digest_job")
    # job_queue.run_repeating(events_digest_job, 10, first=0, name="events_digest_job")
    logging.info("Events digest job started...")


def stop_events_digest_job(job_queue):
    for job in job_queue.get_jobs_by_name("events_digest_job"):
        job.schedule_removal()
        logging.info("Events digest job stopped...")


def events_digest_job(context):
    users = EventsDigestSession.query(User).filter(User.is_active == True).filter(User.region != None).all()

    for user in users:
        today = datetime.date.today()
        _ = user.translator
        regions = list(set([user.region] + [club.region for club in user.clubs]))
        events = EventsDigestSession.query(Event) \
            .join(Event.clubs) \
            .filter(Event.start_date < datetime.datetime.utcnow() + datetime.timedelta(days=21)) \
            .filter(Event.end_date > datetime.datetime.utcnow()) \
            .filter(Club.region_id.in_([r.id for r in regions])) \
            .all()

        if len(regions) > 1:
            message_text = ""
            for region in regions:
                region_events = [e for e in events if (region.id in [c.region_id for c in e.clubs])]
                if not region_events:
                    continue

                message_text += "<b>" + _("🗓️ Events of {region_name} for near 3 weeks").format(region_name=region.get_translation(user.language_code).name_declension) + '</b>\n'
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                for event in region_events:
                    message_text += f"🔸 {event.start_date.strftime('%d.%m.%Y')} {event.title}\n"
                message_text += "\n"
        else:
            message_text = "<b>" + _("🗓️ Events of {region_name} for near 3 weeks").format(region_name=user.region.get_translation(user.language_code).name_declension) + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            for event in events:
                message_text += f"🔸 {event.start_date.strftime('%d.%m.%Y')} {event.title}\n"

        buttons = [[InlineKeyboardButton(_("👀 Read events") + f" ({len(events)})", callback_data=f"read_events_{today.strftime('%d.%m.%Y')}")],
                   [InlineKeyboardButton(_("❎ Close message"), callback_data="close_message")]]
        if events:
            context.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")


def start_offers_digest_job(job_queue):
    stop_offers_digest_job(job_queue)
    settings = get_settings(SETTINGS_FILE)
    job_queue.run_daily(offers_digest_job, datetime.time(settings['offers_digest']['hour'] + SERVER_TIME_CORRECTION, settings['offers_digest']['minute']), (settings['offers_digest']['day'],), name="offers_digest_job")
    # job_queue.run_repeating(offers_digest_job, 10, first=0, name="offers_digest_job")
    logging.info("Offers digest job started...")


def stop_offers_digest_job(job_queue):
    for job in job_queue.get_jobs_by_name("offers_digest_job"):
        job.schedule_removal()
        logging.info("Offers digest job stopped...")


def offers_digest_job(context):
    users = OffersDigestSession.query(User).filter(User.is_active == True).filter(User.region != None).all()

    for user in users:
        today = datetime.date.today()
        _ = user.translator
        regions = list(set([user.region] + [club.region for club in user.clubs]))
        offers = OffersDigestSession.query(Offer) \
            .join(Offer.clubs) \
            .filter(Offer.create_date > datetime.datetime.utcnow() - datetime.timedelta(days=7)) \
            .filter(Club.region_id.in_([r.id for r in regions])) \
            .all()

        if len(regions) > 1:
            message_text = ""
            for region in regions:
                region_offers = [o for o in offers if (region.id in [c.region_id for c in o.clubs])]
                if not region_offers:
                    continue

                message_text += "<b>" + _("🔖️ Offers of {region_name} for last week").format(region_name=region.get_translation(user.language_code).name_declension) + '</b>\n'
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                for offer in region_offers:
                    message_text += f"🔸 {offer.create_date.strftime('%d.%m.%Y')} {offer.title}\n"
                message_text += "\n"
        else:
            message_text = "<b>" + _("🔖️ Offers of {region_name} for last week").format(region_name=user.region.get_translation(user.language_code).name_declension) + '</b>\n'

            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            for offer in offers:
                message_text += f"🔸 {offer.create_date.strftime('%d.%m.%Y')} {offer.title}\n"

        buttons = [[InlineKeyboardButton(_("👀 Read offers") + f" ({len(offers)})", callback_data=f"read_offers_{today.strftime('%d.%m.%Y')}")],
                   [InlineKeyboardButton(_("❎ Close message"), callback_data="close_message")]]
        if offers:
            context.bot.send_message(chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
