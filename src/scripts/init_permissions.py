from src.models import DBSession, Permission, User, Club, JoinClubRequest

# noinspection SpellCheckingInspection
from src.settings import SUPERUSER_ACCOUNTS


def main():
    """"""
    _ = lambda text: text

    """-----------------User-----------------"""
    Permission.create('start_menu_access', _('Access to "Start" menu'))
    Permission.create('profile_menu_access', _('Access to "Profile" menu'))
    Permission.create('change_language_menu_access', _('Access to "Change language" menu'))
    Permission.create('clubs_menu_access', _('Access to "Clubs" menu'))
    Permission.create('join_club_menu_access', _('Access to "Join club" menu'))
    Permission.create('waiting_assignment_menu_access', _('Access to "Waiting assignment" menu'))
    Permission.create('my_clubs_menu_access', _('Access to "My clubs" menu'))
    Permission.create('on_moderation_menu_access', _('Access to "On moderation" menu'))
    Permission.create('digest_menu_access', _('Access to "Digest" menu'))
    Permission.create('view_news_menu_access', _('Access to "View news" menu'))
    Permission.create('view_events_menu_access', _('Access to "View events" menu'))
    Permission.create('view_offers_menu_access', _('Access to "View offers" menu'))
    Permission.create('announcement_menu_access', _('Access to "Announcement" menu'))
    Permission.create('allow_create_news', _('Allow create news'))
    Permission.create('allow_create_events', _('Allow create events'))
    Permission.create('allow_create_offers', _('Allow create offers'))

    """-----------------Admin-----------------"""
    # menus
    Permission.create('admin_menu_access', _('Access to "Admin" menu'))
    Permission.create('distribution_menu_access', _('Access to "Distribution" admin menu'))
    Permission.create('settings_menu_access', _('Access to "Settings" admin menu'))
    Permission.create('statistics_menu_access', _('Access to "Statistics" admin menu'))

    # clubs menu
    Permission.create(Club.view_permission, _('Access to "Clubs" admin menu'))
    Permission.create(Club.add_permission, _('Access to "Add club" admin menu'))
    Permission.create(Club.edit_permission, _('Access to "Edit club" admin menu'))
    Permission.create(Club.delete_permission, _('Allow delete club'))

    # club requests menu
    Permission.create(JoinClubRequest.view_permission, _('Access to "Club requests" admin menu'))
    Permission.create(JoinClubRequest.edit_permission, _('Access to "Edit club request" admin menu'))

    # permissions menu
    Permission.create(Permission.view_permission, _('Access to "Permissions" admin menu'))
    Permission.create(Permission.add_permission, _('Allow add permission'))
    Permission.create(Permission.delete_permission, _('Allow remove permission'))

    Permission.create('superuser', _('Superuser'))

    DBSession.commit()

    users = DBSession.query(User).filter(User.chat_id.in_(SUPERUSER_ACCOUNTS)).all()
    permission = DBSession.query(Permission).get('superuser')
    for user in users:
        if not user.has_permission(permission.code):
            user.permissions.append(permission)
        DBSession.add(user)
    DBSession.commit()


if __name__ == '__main__':
    main()
