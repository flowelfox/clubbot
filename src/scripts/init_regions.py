from src.models import DBSession, Region, RegionTranslation

regions = []


def create_region(name_ru, name_uk, name_declension_ru, name_declension_uk):
    existed_region = DBSession.query(RegionTranslation).filter(RegionTranslation.name == name_ru).first()
    if existed_region is None:
        region = Region()
        region.translations.append(RegionTranslation(lang='ru', region=region, name=name_ru, name_declension=name_declension_ru))
        region.translations.append(RegionTranslation(lang='uk', region=region, name=name_uk, name_declension=name_declension_uk))
        regions.append(region)
    else:
        trans_ru = DBSession.query(RegionTranslation)\
            .filter(RegionTranslation.region == existed_region.region)\
            .filter(RegionTranslation.lang == 'ru')\
            .first()
        trans_uk = DBSession.query(RegionTranslation)\
            .filter(RegionTranslation.region == existed_region.region)\
            .filter(RegionTranslation.lang == 'uk')\
            .first()
        trans_ru.name = name_ru
        trans_ru.name_declension = name_declension_ru
        regions.append(trans_ru)

        trans_uk.name = name_uk
        trans_uk.name_declension = name_declension_uk
        regions.append(trans_uk)


def main():
    create_region("Винницкая", "Вінницька", "Винничины", "Вінниччини")
    create_region("Волынская", "Волинська", "Волынщины", "Волинщини")
    create_region("Днепропетровская", "Дніпропетровська", "Днепропетровщины", "Дніпропетровщини")
    create_region("Донецкая", "Донецька", "Донетчины", "Донеччини")
    create_region("Житомирская", "Житомирська", "Житомирщины", "Житомирщини")
    create_region("Закарпатская", "Закарпатська", "Закарпатщины", "Закарпатщини")
    create_region("Запорожская", "Запорізька", "Запорожчины", "Запоріжчини")
    create_region("Ивано-Франковская", "Івано-Франківська", "Ивано-Франковщины", "Івано-Франківщини")
    create_region("Киевская", "Київська", "Киевщины", "Київщини")
    create_region("Кировоградская", "Кіровоградська", "Кировоградщины", "Кіровоградщини")
    create_region("Луганская", "Луганська", "Луганщины", "Луганщини")
    create_region("Львовская", "Львівська", "Львовщины", "Львівщини")
    create_region("Николаевская", "Миколаївська", "Николаевщины", "Миколаївщини")
    create_region("Одесская", "Одеська", "Одесщины", "Одещини")
    create_region("Полтавская", "Полтавська", "Полтавщины", "Полтавщини")
    create_region("Ровенская", "Рівненська", "Ровенщины", "Рівенщини")
    create_region("Сумская", "Сумська", "Сумщины", "Сумщини")
    create_region("Тернопольская", "Тернопільська", "Тернопольщины", "Тернопільщини")
    create_region("Харьковская", "Харківська", "Харьковщины", "Харківщини")
    create_region("Херсонская", "Херсонська", "Херсонщины", "Херсонщини")
    create_region("Хмельницкая", "Хмельницька", "Хмельницкой", "Хмельниччини")
    create_region("Черкасская", "Черкаська", "Черкасщины", "Черкащини")
    create_region("Черновицкая", "Чернівецька", "Черноветщина", "Чернівеччини")
    create_region("Черниговская", "Чернігівська", "Черниговщины", "Чернігівщини")

    if regions:
        DBSession.add_all(regions)
        DBSession.commit()


if __name__ == '__main__':
    main()
