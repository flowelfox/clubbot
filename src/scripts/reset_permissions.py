import logging

from src.models import DBSession, User

logger = logging.getLogger(__name__)


def main():
    users = DBSession.query(User).filter(User.is_active == True).all()

    for user in users:
        user.init_permissions()
        DBSession.add(user)
        logger.info(f"Default permissions set for {user.get_name()}")
    DBSession.commit()


if __name__ == '__main__':
    main()
